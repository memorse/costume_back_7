package com.wanxi.service;

import com.wanxi.dao.DesignerDao;
import com.wanxi.dao.DesignerLevelDao;
import com.wanxi.daoImpl.DesignerLevelDaoIpml;
import com.wanxi.model.DesignerLevelModel;
import com.wanxi.util.JedisUtil;
import com.wanxi.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;

public class DesignerLevelService implements DesignerLevelDao {
    @Override
    public List<DesignerLevelModel> getDesignerLevels() {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<DesignerLevelModel> list=new ArrayList<>();
        Jedis jedis = new Jedis();
        try {
            List<String> designer_level_id = jedis.lrange("designer_level_id", 0, -1);
            if(designer_level_id.size()>0){
                for(int i=0;i<designer_level_id.size();i++){
                    DesignerLevelModel designerLevelModel = new DesignerLevelModel();
                    designerLevelModel.setId(Integer.valueOf(designer_level_id.get(i)));
                    designerLevelModel.setLevelName(jedis.lrange("designer_level_name",0,-1).get(i));
                    list.add(designerLevelModel);
                }

            }else {
                list=sqlSession.getMapper(DesignerLevelDao.class).getDesignerLevelsByKeyWithPage("",0,10,0);
                for (DesignerLevelModel designerLevelModel : list) {
                    jedis.rpush("designer_level_id",designerLevelModel.getId()+"");
                    jedis.rpush("designer_level_name",designerLevelModel.getLevelName()+"");
                }

            }

        }catch (Exception e){

        }finally {
            jedis.close();
            sqlSession.close();
        }
        return list;
    }

    @Override
    public List<DesignerLevelModel> getDesignerLevelsByKey(String key) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<DesignerLevelModel> designerLevelsByKey = sqlSession.getMapper(DesignerLevelDao.class).getDesignerLevelsByKey(key);
        sqlSession.close();
        return designerLevelsByKey;
    }

    @Override
    public List<DesignerLevelModel> getDesignerLevelsByKeyWithPage(String key, int startPage, int pageSize, int isPage) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<DesignerLevelModel> designerLevelsByKeyWithPage = sqlSession.getMapper(DesignerLevelDao.class).getDesignerLevelsByKeyWithPage(key, startPage, pageSize, isPage);
        sqlSession.close();
        return designerLevelsByKeyWithPage;
    }

    @Override
    public DesignerLevelModel getDesignerLevelById(int id) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        DesignerLevelModel designerLevelModel = new DesignerLevelModel();
        Jedis jedis = new Jedis();
        try {
            List<String> designer_level_id = jedis.lrange("designer_level_id", 0, -1);
            if(designer_level_id.size()>0){
                for(int i=0;i<designer_level_id.size();i++){
                    if(id==Integer.valueOf(designer_level_id.get(i))){
                        designerLevelModel.setId(Integer.valueOf(designer_level_id.get(i)));
                        designerLevelModel.setLevelName(jedis.lrange("designer_level_name",0,-1).get(i));
                        return designerLevelModel;
                    }
                }

            }
            designerLevelModel=sqlSession.getMapper(DesignerLevelDao.class).getDesignerLevelById(id);
            jedis.rpush("designer_level_id",designerLevelModel.getId()+"");
            jedis.rpush("designer_level_name",designerLevelModel.getLevelName()+"");
        }catch (Exception e){

        }finally {
            jedis.close();
            sqlSession.close();
        }
        return designerLevelModel;
    }

    @Override
    public int addDesignerLevel(DesignerLevelModel designerLevel) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i= sqlSession.getMapper(DesignerLevelDao.class).addDesignerLevel(designerLevel);
        if(i>0){
            JedisUtil.delByKey("designer_level_id","designer_level_name");
        }
        sqlSession.close();
        return i;
    }

    @Override
    public int updateDesignerLevel(DesignerLevelModel designerLevel) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i = sqlSession.getMapper(DesignerLevelDao.class).updateDesignerLevel(designerLevel);
        if(i>0){
            JedisUtil.delByKey("designer_level_id","designer_level_name");
        }
        sqlSession.close();
        return i ;
    }

    @Override
    public int delDesignerLevel(int id) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i= sqlSession.getMapper(DesignerLevelDao.class).delDesignerLevel(id);
        if(i>0){
            JedisUtil.delByKey("designer_level_id","designer_level_name");
        }
        sqlSession.close();
        return i ;
    }
}
