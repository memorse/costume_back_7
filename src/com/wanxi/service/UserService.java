package com.wanxi.service;

import com.wanxi.dao.UserDao;
import com.wanxi.daoImpl.UserDaoImpl;
import com.wanxi.model.SearchDataModel;
import com.wanxi.model.UserModel;
import com.wanxi.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class UserService implements UserDao {
    @Override
    public int delUser(String id) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i = sqlSession.getMapper(UserDao.class).delUser(id);
        sqlSession.commit();
        sqlSession.close();
        return i;
    }

    @Override
    public UserModel getUserByUesrAccount(String userAccount) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        UserModel userByUesrAccount = sqlSession.getMapper(UserDao.class).getUserByUesrAccount(userAccount);
        sqlSession.close();
        return userByUesrAccount;
    }

    @Override
    public List<UserModel> getUsers() {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<UserModel> users = sqlSession.getMapper(UserDao.class).getUsers();
        sqlSession.close();
        return users;
    }

    @Override
    public List<UserModel> getUsersByKey(String key) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<UserModel> usersByKey = sqlSession.getMapper(UserDao.class).getUsersByKey(key);
        sqlSession.close();
        return usersByKey;
    }

    public List<UserModel> getUsersByKeyWithPage(String key,int startPage,int pageSize) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<UserModel> usersByKeyWithPage = sqlSession.getMapper(UserDao.class).getUsersByKeyWithPage(key, startPage, pageSize);
        sqlSession.close();
        return usersByKeyWithPage;
    }

    public List<UserModel> getUsersByKeyWithPagePlus(SearchDataModel searchDataModel,int isPage) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<UserModel> usersByKeyWithPagePlus = sqlSession.getMapper(UserDao.class).getUsersByKeyWithPagePlus(searchDataModel, isPage);
        sqlSession.close();
        return usersByKeyWithPagePlus;
    }

    @Override
    public int saveUser(UserModel user) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i = sqlSession.getMapper(UserDao.class).saveUser(user);
        sqlSession.close();
        return i;
    }

    @Override
    public int updateUser(UserModel user) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i = sqlSession.getMapper(UserDao.class).updateUser(user);
        sqlSession.close();
        return i;
    }

    public UserModel verifyLogin(String username, String password){
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        UserModel userModel = sqlSession.getMapper(UserDao.class).verifyLogin(username, password);
        sqlSession.close();
        return userModel;
    }
}
