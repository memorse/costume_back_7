package com.wanxi.service;

import com.wanxi.dao.NavigatorDao;
import com.wanxi.daoImpl.NavigatorDaoImpl;
import com.wanxi.model.NavigatorModel;
import com.wanxi.util.JedisUtil;
import com.wanxi.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;

public class NavigatorService implements NavigatorDao {

    @Override
    public List<NavigatorModel> getNavigators() {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<NavigatorModel> navigators = sqlSession.getMapper(NavigatorDao.class).getNavigators();
        sqlSession.close();
        return navigators;
    }

    @Override
    public List<NavigatorModel> getNavigatorsByKey(String key) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<NavigatorModel> navigatorsByKey = sqlSession.getMapper(NavigatorDao.class).getNavigatorsByKey(key);
        sqlSession.close();
        return navigatorsByKey;
    }

    @Override
    public List<NavigatorModel> getNavigatorsByKeyWithPage(String key, int startPage, int pageSize, int isPage) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<NavigatorModel> navigatorsByKeyWithPage = sqlSession.getMapper(NavigatorDao.class).getNavigatorsByKeyWithPage(key, startPage, pageSize, isPage);
        sqlSession.close();
        return navigatorsByKeyWithPage;
    }

    @Override
    public int saveNavigator(NavigatorModel navigatorModel) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i= sqlSession.getMapper(NavigatorDao.class).saveNavigator(navigatorModel);
        if(i>0)
            JedisUtil.delByKey(
                    "front_nav_ids",
                    "front_nav_enables",
                    "front_nav_levels",
                    "front_nav_parents",
                    "front_nav_targets",
                    "front_nav_names"
            );
        sqlSession.close();
        return i;
    }

    @Override
    public int updateNavigator(NavigatorModel navigatorModel) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i = sqlSession.getMapper(NavigatorDao.class).updateNavigator(navigatorModel);
        if(i>0)
            JedisUtil.delByKey(
                    "front_nav_ids",
                    "front_nav_enables",
                    "front_nav_levels",
                    "front_nav_parents",
                    "front_nav_targets",
                    "front_nav_names"
                    );
        sqlSession.close();
        return i;
    }

    @Override
    public int delNavigator(int id) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i= sqlSession.getMapper(NavigatorDao.class).delNavigator(id);
        if(i>0)
            JedisUtil.delByKey(
                    "front_nav_ids",
                    "front_nav_enables",
                    "front_nav_levels",
                    "front_nav_parents",
                    "front_nav_targets",
                    "front_nav_names"
            );
        sqlSession.close();
        return i;
    }

    public List<NavigatorModel> getNavigatorsForIndex() {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<NavigatorModel> list=new ArrayList<>();
        Jedis jedis =null;
        try {
            jedis=new Jedis();
            List<String> front_nav_ids = jedis.lrange("front_nav_ids", 0, -1);
            if(front_nav_ids.size()>0){
                for(int i=0;i<front_nav_ids.size();i++){
                    NavigatorModel navigatorModel = new NavigatorModel();
//                    navigatorModel.setEnable(Integer.valueOf(jedis.lrange("front_nav_hrefs",0,-1).get(i)));
                    navigatorModel.setId(Integer.valueOf(front_nav_ids.get(i)));
                    navigatorModel.setParent(Integer.valueOf(jedis.lrange("front_nav_parents",0,-1).get(i)));
                    navigatorModel.setLevel(Integer.valueOf(jedis.lrange("front_nav_levels",0,-1).get(i)));
                    navigatorModel.setTarget(jedis.lrange("front_nav_targets",0,-1).get(i));
                    navigatorModel.setName(jedis.lrange("front_nav_names",0,-1).get(i));
                    navigatorModel.setEnable(Integer.valueOf(jedis.lrange("front_nav_enables",0,-1).get(i)));

                    list.add(navigatorModel);
                }

            }else {
                list=sqlSession.getMapper(NavigatorDao.class).getNavigatorsForIndex();
                for (NavigatorModel navigatorModel : list) {
                    jedis.rpush("front_nav_ids",navigatorModel.getId()+"");
                    jedis.rpush("front_nav_enables",navigatorModel.getEnable()+"");
                    jedis.rpush("front_nav_levels",navigatorModel.getLevel()+"");
                    jedis.rpush("front_nav_parents",navigatorModel.getParent()+"");
                    jedis.rpush("front_nav_targets",navigatorModel.getTarget());
                    jedis.rpush("front_nav_names",navigatorModel.getName());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(jedis!=null)
                jedis.close();
            sqlSession.close();
        }

        return list;
    }
}
