package com.wanxi.service;

import com.wanxi.dao.NewsDao;
import com.wanxi.daoImpl.NewsDaoImpl;
import com.wanxi.model.NewsModel;
import com.wanxi.util.JedisUtil;
import com.wanxi.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import redis.clients.jedis.Jedis;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NewsService implements NewsDao {

    @Override
    public List<NewsModel> getNews() {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<NewsModel> news = sqlSession.getMapper(NewsDao.class).getNews();
        sqlSession.close();
        return news;
    }

    @Override
    public List<NewsModel> getNewsByKey(String key) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<NewsModel> newsByKey = sqlSession.getMapper(NewsDao.class).getNewsByKey(key);
        sqlSession.close();
        return newsByKey;
    }

    @Override
    public List<NewsModel> getNewsByKeyWithPage(String key, int startPage, int pageSize, int isPage) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<NewsModel> newsByKeyWithPage = sqlSession.getMapper(NewsDao.class).getNewsByKeyWithPage(key, startPage, pageSize, isPage);
        sqlSession.close();
        return newsByKeyWithPage;
    }


    @Override
    public int saveNews(NewsModel newsModel) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i= sqlSession.getMapper(NewsDao.class).saveNews(newsModel);
        if(i>0)
            JedisUtil.delByKey(
                    "front_news_ids",
                    "front_news_pubtime",
                    "front_news_enable",
                    "front_news_content",
                    "front_news_images",
                    "front_news_title",
                    "front_news_sort");
        sqlSession.close();
        return i;
    }

    @Override
    public int updateNews(NewsModel newsModel) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i = sqlSession.getMapper(NewsDao.class).updateNews(newsModel);
        if(i>0)
            JedisUtil.delByKey(
                    "front_news_ids",
                    "front_news_pubtime",
                    "front_news_enable",
                    "front_news_content",
                    "front_news_images",
                    "front_news_title",
                    "front_news_sort");
        sqlSession.close();
        return i;
    }

    @Override
    public int delNews(int id){
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i= sqlSession.getMapper(NewsDao.class).delNews(id);
        if(i>0)
            JedisUtil.delByKey(
                    "front_news_ids",
                    "front_news_pubtime",
                    "front_news_enable",
                    "front_news_content",
                    "front_news_images",
                    "front_news_title",
                    "front_news_sort");
        sqlSession.close();
        return i;
    }

    public List<NewsModel> getNewsForIndex() {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<NewsModel> list=new ArrayList<>();
        Jedis jedis = new Jedis();
        try {
            List<String> front_news_ids = jedis.lrange("front_news_ids", 0, -1);
            if(front_news_ids.size()>0){

                for (int i=0;i<front_news_ids.size();i++){
                    NewsModel newsModel = new NewsModel();
                    newsModel.setNewsSort(new NewsSortService().
                            getNewsSortsById(Integer.valueOf(jedis.lrange("front_news_sort",0,-1).get(i))));
                    newsModel.setEnable(Integer.valueOf(jedis.lrange("front_news_enable",0,-1).get(i)));
                    newsModel.setId(Integer.valueOf(front_news_ids.get(i)));
                    SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
                    String front_news_pubtime = jedis.lrange("front_news_pubtime", 0, -1).get(i);
                    Date date = simpleDateFormat.parse(front_news_pubtime);
                    newsModel.setPubTime(date);
                    newsModel.setImgRoute(jedis.lrange("front_news_images",0,-1).get(i));
                    newsModel.setContent(jedis.lrange("front_news_content",0,-1).get(i));
                    newsModel.setTitle(jedis.lrange("front_news_title",0,-1).get(i));
                    list.add(newsModel);
                }

            }else {
                list = sqlSession.getMapper(NewsDao.class).getNewsForIndex();
                for (NewsModel newsModel : list) {
                    jedis.lpush("front_news_sort",newsModel.getNewsSort().getId()+"");
                    jedis.lpush("front_news_enable",newsModel.getEnable()+"");
                    jedis.lpush("front_news_content",newsModel.getContent());
                    jedis.lpush("front_news_images",newsModel.getImgRoute());
                    jedis.lpush("front_news_title",newsModel.getTitle());
                    jedis.lpush("front_news_pubtime",new SimpleDateFormat("yyyy-MM-dd:HH:mm:ss").format(newsModel.getPubTime()));
                    jedis.lpush("front_news_ids",newsModel.getId()+"");
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            jedis.close();
            sqlSession.close();
        }
        return list;
    }
}
