package com.wanxi.service;

import com.wanxi.dao.NewsSortDao;
import com.wanxi.daoImpl.NewsSortDaoImpl;
import com.wanxi.model.NewsSortModel;
import com.wanxi.util.JedisUtil;
import com.wanxi.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;

public class NewsSortService implements NewsSortDao {
    @Override
    public List<NewsSortModel> getNewsSorts() {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<NewsSortModel> newsSorts = sqlSession.getMapper(NewsSortDao.class).getNewsSorts();
        sqlSession.close();
        return newsSorts;
    }

    @Override
    public List<NewsSortModel> getNewsSortsByKey(String key) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<NewsSortModel> newsSortsByKey = sqlSession.getMapper(NewsSortDao.class).getNewsSortsByKey(key);
        sqlSession.close();
        return newsSortsByKey;
    }

    @Override
    public List<NewsSortModel> getNewsSortsByKeyWithPage(String key, int startPage, int pageSize, int isPage) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<NewsSortModel> newsSortsByKeyWithPage = sqlSession.getMapper(NewsSortDao.class).getNewsSortsByKeyWithPage(key, startPage, pageSize, isPage);
        sqlSession.close();
        return newsSortsByKeyWithPage;
    }

    @Override
    public NewsSortModel getNewsSortsById(int id) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        Jedis jedis=new Jedis();
        NewsSortModel newsSortModel=new NewsSortModel();
        try {
            List<String> newsSortId = jedis.lrange("front_newsSort_id", 0, -1);
            if(newsSortId.size()>0){
                for (int i=0;i<newsSortId.size();i++) {
                    if(id==Integer.valueOf(newsSortId.get(i))){
                        newsSortModel.setId(Integer.valueOf(newsSortId.get(i)));
                        newsSortModel.setEnable(Integer.valueOf(jedis.lrange("front_newsSort_enable",0,-1).get(i)));
                        newsSortModel.setType(jedis.lrange("front_newsSort_type",0,-1).get(i));
                        newsSortModel.setPriKey(jedis.lrange("front_newsSort_prikey",0,-1).get(i));
                        return newsSortModel;
                    }
                }
            }
                newsSortModel = sqlSession.getMapper(NewsSortDao.class).getNewsSortsById(id);
                jedis.rpush("front_newsSort_id",newsSortModel.getId()+"");
                jedis.rpush("front_newsSort_enable",newsSortModel.getEnable()+"");
                jedis.rpush("front_newsSort_type",newsSortModel.getType()+"");
                jedis.rpush("front_newsSort_prikey",newsSortModel.getPriKey()+"");

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            jedis.close();
            sqlSession.close();
        }

        return newsSortModel;
    }

    @Override
    public int saveNewsSort(NewsSortModel newsSortModel) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i= sqlSession.getMapper(NewsSortDao.class).saveNewsSort(newsSortModel);
        if(i>0)
            JedisUtil.delByKey(
                    "front_newsSort_id",
                    "front_newsSort_enable",
                    "front_newsSort_prikey",
                    "front_newsSort_type"
            );
        sqlSession.close();
        return i;
    }

    @Override
    public int updateNewsSort(NewsSortModel newsSortModel) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i = sqlSession.getMapper(NewsSortDao.class).updateNewsSort(newsSortModel);
        if(i>0)
            JedisUtil.delByKey(
                    "front_newsSort_id",
                    "front_newsSort_enable",
                    "front_newsSort_prikey",
                    "front_newsSort_type"
                    );
        sqlSession.close();
        return i;
    }

    @Override
    public int delNewsSort(int id) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i =sqlSession.getMapper(NewsSortDao.class).delNewsSort(id);
        if(i>0)
            JedisUtil.delByKey(
                    "front_newsSort_id",
                    "front_newsSort_enable",
                    "front_newsSort_prikey",
                    "front_newsSort_type"
            );
        sqlSession.close();
        return i;
    }

    public List<NewsSortModel> getNewsSortsForIndex() {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<NewsSortModel> list=new ArrayList<>();
        Jedis jedis = new Jedis();
        try {
            List<String> front_newsSort_id = jedis.lrange("front_newsSort_id", 0, -1);
            if(front_newsSort_id.size()>0){
                for(int i=0;i<front_newsSort_id.size();i++){
                    NewsSortModel newsSortModel = new NewsSortModel();
                    newsSortModel.setId(Integer.valueOf(front_newsSort_id.get(i)));
                    newsSortModel.setEnable(Integer.valueOf(jedis.lrange("front_newsSort_enable",0,-1).get(i)));
                    newsSortModel.setPriKey(jedis.lrange("front_newsSort_prikey",0,-1).get(i));
                    newsSortModel.setType(jedis.lrange("front_newsSort_type",0,-1).get(i));
                    list.add(newsSortModel);
                }
            }else {
                list=sqlSession.getMapper(NewsSortDao.class).getNewsSortsForIndex();
                for (NewsSortModel newsSortModel : list) {
                    jedis.lpush("front_newsSort_id",newsSortModel.getId()+"");
                    jedis.lpush("front_newsSort_enable",newsSortModel.getEnable()+"");
                    jedis.lpush("front_newsSort_prikey",newsSortModel.getPriKey()+"");
                    jedis.lpush("front_newsSort_type",newsSortModel.getType()+"");
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            jedis.close();
            sqlSession.close();
        }
        return list;
    }
}
