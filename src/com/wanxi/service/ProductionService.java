package com.wanxi.service;

import com.wanxi.dao.ProductionDao;
import com.wanxi.daoImpl.ProductionDaoImpl;
import com.wanxi.daoImpl.ProductionTypeDaoImpl;
import com.wanxi.model.ProductionModel;
import com.wanxi.util.JedisUtil;
import com.wanxi.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;

public class ProductionService implements ProductionDao {
    @Override
    public List<ProductionModel> getProductions() {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<ProductionModel> productions = sqlSession.getMapper(ProductionDao.class).getProductions();
        sqlSession.close();
        return productions;
    }

    @Override
    public List<ProductionModel> getProductionsBykey(String key) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<ProductionModel> productionsBykey = sqlSession.getMapper(ProductionDao.class).getProductionsBykey(key);
        sqlSession.close();
        return productionsBykey;
    }

    @Override
    public List<ProductionModel> getProductionsBykeyWithPage(String key, int startPage, int pageSize, int isPage) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<ProductionModel> productionsBykeyWithPage = sqlSession.getMapper(ProductionDao.class).getProductionsBykeyWithPage(key, startPage, pageSize, isPage);
        sqlSession.close();
        return productionsBykeyWithPage;
    }

    public List<ProductionModel> getProductionsByType(int id) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<ProductionModel> productionsByType = sqlSession.getMapper(ProductionDao.class).getProductionsByType(id);
        sqlSession.close();
        return productionsByType;
    }

    @Override
    public int saveProduction(ProductionModel pro) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i= sqlSession.getMapper(ProductionDao.class).saveProduction(pro);
        if(i>0)
            JedisUtil.delByKey(
                    "front_production_id",
                    "front_production_enable",
                    "front_production_photo",
                    "front_production_name",
                    "front_production_price",
                    "front_production_type",
                    "front_production_designer"
            );
        sqlSession.close();
        return i;
    }

    @Override
    public int updateProduction(ProductionModel pro) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i = sqlSession.getMapper(ProductionDao.class).updateProduction(pro);
        if(i>0)
            JedisUtil.delByKey(
                    "front_production_id",
                    "front_production_enable",
                    "front_production_photo",
                    "front_production_name",
                    "front_production_price",
                    "front_production_type",
                    "front_production_designer"
                    );
        sqlSession.close();
        return i;
    }

    @Override
    public int delProduction(int id) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i = sqlSession.getMapper(ProductionDao.class).delProduction(id);
        if(i>0)
            JedisUtil.delByKey(
                    "front_production_id",
                    "front_production_enable",
                    "front_production_photo",
                    "front_production_name",
                    "front_production_price",
                    "front_production_type",
                    "front_production_designer"
            );
        sqlSession.close();
        return i;
    }

    @Override
    public List<ProductionModel> getProductionsForIndex() {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<ProductionModel> list=new ArrayList<>();
        Jedis jedis = new Jedis();
        try {
            List<String> front_production_id = jedis.lrange("front_production_id", 0, -1);
            if(front_production_id.size()>0){
                for(int i=0;i<front_production_id.size();i++){
                    ProductionModel productionModel = new ProductionModel();
                    productionModel.setId(Integer.valueOf(front_production_id.get(i)));
                    productionModel.setEnable(Integer.valueOf(jedis.lrange("front_production_enable",0,-1).get(i)));
                    productionModel.setProPhoto(jedis.lrange("front_production_photo",0,-1).get(i));
                    productionModel.setProName(jedis.lrange("front_production_name",0,-1).get(i));
                    productionModel.setProPrice(jedis.lrange("front_production_price",0,-1).get(i));
                    productionModel.setType(new ProductionTypeDaoImpl().
                            getProductionTypeById(Integer.valueOf(jedis.lrange("front_production_type",0,-1).get(i))));
                    productionModel.setDesigner(new DesignerService().
                            getDesignersById(Integer.valueOf(jedis.lrange("front_production_designer",0,-1).get(i))));
                    list.add(productionModel);
                }
            }else {
                list=sqlSession.getMapper(ProductionDao.class).getProductionsForIndex();
                for (ProductionModel productionModel : list) {
                    jedis.lpush("front_production_id",productionModel.getId()+"");
                    jedis.lpush("front_production_enable",productionModel.getEnable()+"");
                    jedis.lpush("front_production_photo",productionModel.getProPhoto()+"");
                    jedis.lpush("front_production_name",productionModel.getProName()+"");
                    jedis.lpush("front_production_price",productionModel.getProPrice()+"");
                    jedis.lpush("front_production_type",productionModel.getType().getId()+"");
                    jedis.lpush("front_production_designer",productionModel.getDesigner().getId()+"");
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            jedis.close();
            sqlSession.close();
        }
        return list;
    }
}
