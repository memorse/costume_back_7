package com.wanxi.service;

import com.wanxi.dao.OfflineStoreDao;
import com.wanxi.daoImpl.OfflineStoreDaoImpl;
import com.wanxi.model.OfflineStoreModel;
import com.wanxi.util.JedisUtil;
import com.wanxi.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;

public class OfflineStoreService implements OfflineStoreDao {

    @Override
    public List<OfflineStoreModel> getOfflineStores() {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<OfflineStoreModel> offlineStores = sqlSession.getMapper(OfflineStoreDao.class).getOfflineStores();
        sqlSession.close();
        return offlineStores;
    }

    @Override
    public List<OfflineStoreModel> getOfflineStoresByKey(String key) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<OfflineStoreModel> offlineStoresByKey = sqlSession.getMapper(OfflineStoreDao.class).getOfflineStoresByKey(key);
        sqlSession.close();
        return offlineStoresByKey;
    }

    @Override
    public List<OfflineStoreModel> getOfflineStoresByKeyWithPage(String key, int startPage, int pageSize, int isPage) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<OfflineStoreModel> offlineStoresByKeyWithPage = sqlSession.getMapper(OfflineStoreDao.class).getOfflineStoresByKeyWithPage(key, startPage, pageSize, isPage);
        sqlSession.close();
        return offlineStoresByKeyWithPage;
    }


    @Override
    public int savaOfflineStoreModel(OfflineStoreModel offlineStore) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i= sqlSession.getMapper(OfflineStoreDao.class).savaOfflineStoreModel(offlineStore);
        if(i>0)
            JedisUtil.delByKey(
                    "Offline_store_id",
                    "Offline_store_enable",
                    "Offline_store_phone",
                    "Offline_store_name",
                    "Offline_store_address",
                    "Offline_store_photo"
                    );
        sqlSession.close();
        return i;
    }

    @Override
    public int updateOfflineStoreModel(OfflineStoreModel offlineStore) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i= sqlSession.getMapper(OfflineStoreDao.class).updateOfflineStoreModel(offlineStore);
        if(i>0)
            JedisUtil.delByKey(
                    "Offline_store_id",
                    "Offline_store_enable",
                    "Offline_store_phone",
                    "Offline_store_name",
                    "Offline_store_address",
                    "Offline_store_photo"
            );
        sqlSession.close();
        return i;
    }

    @Override
    public int delOfflineStoreModel(int id) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i= sqlSession.getMapper(OfflineStoreDao.class).delOfflineStoreModel(id);
        if(i>0)
            JedisUtil.delByKey(
                    "Offline_store_id",
                    "Offline_store_enable",
                    "Offline_store_phone",
                    "Offline_store_name",
                    "Offline_store_address",
                    "Offline_store_photo"
            );
        sqlSession.close();
        return i;
    }

    public List<OfflineStoreModel> getOfflineStoresForIndex() {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<OfflineStoreModel> list=new ArrayList<>();
        Jedis jedis = new Jedis();
        try {
            List<String> offline_store_id = jedis.lrange("Offline_store_id", 0, -1);
            if(offline_store_id.size()>0){
                for(int i=0;i<offline_store_id.size();i++){
                    OfflineStoreModel offlineStoreModel = new OfflineStoreModel();
                    offlineStoreModel.setId(Integer.valueOf(offline_store_id.get(i)));
                    offlineStoreModel.setEnable(Integer.valueOf(jedis.lrange("Offline_store_enable",0,-1).get(i)));
                    offlineStoreModel.setPhone(jedis.lrange("Offline_store_phone",0,-1).get(i));
                    offlineStoreModel.setName(jedis.lrange("Offline_store_name",0,-1).get(i));
                    offlineStoreModel.setAddress(jedis.lrange("Offline_store_address",0,-1).get(i));
                    offlineStoreModel.setPhoto(jedis.lrange("Offline_store_photo",0,-1).get(i));
                    list.add(offlineStoreModel);
                }
            }else {
               list= sqlSession.getMapper(OfflineStoreDao.class).getOfflineStoresForIndex();
                for (OfflineStoreModel offlineStoreModel : list) {
                    jedis.rpush("Offline_store_id",offlineStoreModel.getId()+"");
                    jedis.rpush("Offline_store_enable",offlineStoreModel.getEnable()+"");
                    jedis.rpush("Offline_store_phone",offlineStoreModel.getPhone()+"");
                    jedis.rpush("Offline_store_name",offlineStoreModel.getName()+"");
                    jedis.rpush("Offline_store_address",offlineStoreModel.getAddress()+"");
                    jedis.rpush("Offline_store_photo",offlineStoreModel.getPhoto());
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            jedis.close();
            sqlSession.close();
        }
        return list ;
    }
}
