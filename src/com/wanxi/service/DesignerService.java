package com.wanxi.service;

import com.wanxi.dao.DesignerDao;
import com.wanxi.daoImpl.DesignerDaoImpl;
import com.wanxi.model.DesignerModel;
import com.wanxi.util.JedisUtil;
import com.wanxi.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;

public class DesignerService implements DesignerDao {

    @Override
    public List<DesignerModel> getDesigners() {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<DesignerModel> designers = sqlSession.getMapper(DesignerDao.class).getDesigners();
        sqlSession.close();
        return designers;
    }

    @Override
    public List<DesignerModel> getDesignersByKey(String key) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<DesignerModel> designersByKey = new DesignerDaoImpl().getDesignersByKey(key);
        sqlSession.close();
        return designersByKey;
    }

    @Override
    public List<DesignerModel> getDesignersByKeyWithPage(String key, int startPage, int pageSize, int isPage) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<DesignerModel> designersByKeyWithPage = sqlSession.getMapper(DesignerDao.class).getDesignersByKeyWithPage(key, startPage, pageSize, isPage);
        sqlSession.close();
        return designersByKeyWithPage;
    }

    @Override
    public int saveDesigner(DesignerModel designerModel) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i= sqlSession.getMapper(DesignerDao.class).saveDesigner(designerModel);
        if(i>0)
            JedisUtil.delByKey(
                    "front_designer_id",
                    "front_designer_enable",
                    "front_designer_introduce",
                    "front_designer_name",
                    "front_designer_gender",
                    "front_designer_level",
                    "front_designer_photo"
            );
        sqlSession.close();
        return i;
    }

    @Override
    public int updateDesigner(DesignerModel designerModel) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i=sqlSession.getMapper(DesignerDao.class).updateDesigner(designerModel);
        if(i>0)
            JedisUtil.delByKey(
                    "front_designer_id",
                    "front_designer_enable",
                    "front_designer_introduce",
                    "front_designer_name",
                    "front_designer_gender",
                    "front_designer_level",
                    "front_designer_photo"
                    );
        sqlSession.close();
        return i;
    }

    @Override
    public int delDesigner(int id) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i= sqlSession.getMapper(DesignerDao.class).delDesigner(id);
        if(i>0)
            JedisUtil.delByKey(
                    "front_designer_id",
                    "front_designer_enable",
                    "front_designer_introduce",
                    "front_designer_name",
                    "front_designer_gender",
                    "front_designer_level",
                    "front_designer_photo"
            );
        sqlSession.close();
        return i;
    }

    @Override
    public DesignerModel getDesignersById(int id) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        DesignerModel designersById = sqlSession.getMapper(DesignerDao.class).getDesignersById(id);
        sqlSession.close();
        return designersById;
    }

    public List<DesignerModel> getDesignersForIndex() {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<DesignerModel> list=new ArrayList<>();
        Jedis jedis = new Jedis();
        try {
            List<String> front_designer_id = jedis.lrange("front_designer_id", 0, -1);
            if(front_designer_id.size()>0){
                for(int i=0;i<front_designer_id.size();i++){
                    DesignerModel designerModel = new DesignerModel();
                    designerModel.setEnable(Integer.valueOf(jedis.lrange("front_designer_enable",0,-1).get(i)));
                    designerModel.setId(Integer.valueOf(front_designer_id.get(i)));
                    designerModel.setIntroduce(jedis.lrange("front_designer_introduce",0,-1).get(i));
                    designerModel.setPhoto(jedis.lrange("front_designer_photo",0,-1).get(i));
                    designerModel.setGender(jedis.lrange("front_designer_gender",0,-1).get(i));
                    designerModel.setName(jedis.lrange("front_designer_name",0,-1).get(i));
                    designerModel.setLevel(new DesignerLevelService().
                            getDesignerLevelById(Integer.valueOf(jedis.lrange("front_designer_level",0,-1).get(i))));
                list.add(designerModel);
                }
            }else {
                list=sqlSession.getMapper(DesignerDao.class).getDesignersForIndex();
                for (DesignerModel designerModel : list) {
                    jedis.rpush("front_designer_id",designerModel.getId()+"");
                    jedis.rpush("front_designer_enable",designerModel.getEnable()+"");
                    jedis.rpush("front_designer_introduce",designerModel.getIntroduce()+"");
                    jedis.rpush("front_designer_photo",designerModel.getPhoto()+"");
                    jedis.rpush("front_designer_gender",designerModel.getGender()+"");
                    jedis.rpush("front_designer_name",designerModel.getName()+"");
                    jedis.rpush("front_designer_level",designerModel.getLevel().getId()+"");
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            jedis.close();
            sqlSession.close();
        }
        return list;
    }
}
