package com.wanxi.service;

import com.wanxi.dao.ProductionTypeDao;
import com.wanxi.model.ProductionTypeModel;
import com.wanxi.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

public class ProductionTypeService implements ProductionTypeDao {

    @Override
    public List<ProductionTypeModel> getProductionTypes() {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<ProductionTypeModel> productionTypes = sqlSession.getMapper(ProductionTypeDao.class).getProductionTypes();
        sqlSession.close();
        return productionTypes;
    }

    @Override
    public ProductionTypeModel getProductionTypeById(int id) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        ProductionTypeModel productionTypeById = sqlSession.getMapper(ProductionTypeDao.class).getProductionTypeById(id);
        sqlSession.close();
        return productionTypeById;
    }
}
