package com.wanxi.service;

import com.wanxi.dao.CustomMessageDao;
import com.wanxi.dao.CustomeMadeDao;
import com.wanxi.daoImpl.CustomMessageDaoImpl;
import com.wanxi.model.CustomMessageModel;
import com.wanxi.util.JedisUtil;
import com.wanxi.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;

public class CustomMessageService implements CustomMessageDao {

    @Override
    public List<CustomMessageModel> getCustomMessages() {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<CustomMessageModel> customMessages = sqlSession.getMapper(CustomMessageDao.class).getCustomMessages();
        sqlSession.close();
        return customMessages;
    }

    @Override
    public List<CustomMessageModel> getCustomMsgByKey(String key) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<CustomMessageModel> customMsgByKey = sqlSession.getMapper(CustomMessageDao.class).getCustomMsgByKey(key);
        sqlSession.close();
        return customMsgByKey;
    }

    @Override
    public List<CustomMessageModel> getCustomMsgByKeyWithPage(String key, int startPage, int pageSize, int isPage) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<CustomMessageModel> customMsgByKeyWithPage = sqlSession.getMapper(CustomMessageDao.class).getCustomMsgByKeyWithPage(key, startPage, pageSize, isPage);
        sqlSession.close();
        return customMsgByKeyWithPage;
    }
    @Override
    public int saveCustomMessage(CustomMessageModel customMessageModel) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i= sqlSession.getMapper(CustomMessageDao.class).saveCustomMessage(customMessageModel);
        if(i>0)
            JedisUtil.delByKey(
                    "front_cms_id",
                    "front_cms_enable",
                    "front_cms_photo",
                    "front_cms_wifeMSG",
                    "front_cms_husMSG",
                    "front_cms_firstName"
            );
        sqlSession.close();
        return i;
    }

    @Override
    public int updateCustomMessage(CustomMessageModel customMessageModel) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i = sqlSession.getMapper(CustomMessageDao.class).updateCustomMessage(customMessageModel);
        if(i>0)
            JedisUtil.delByKey(
                    "front_cms_id",
                    "front_cms_enable",
                    "front_cms_photo",
                    "front_cms_wifeMSG",
                    "front_cms_husMSG",
                    "front_cms_firstName"
                    );
        sqlSession.close();
        return i;
    }

    @Override
    public int delCustomMessage(int id) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i= sqlSession.getMapper(CustomMessageDao.class).delCustomMessage(id);
        if(i>0)
            JedisUtil.delByKey(
                    "front_cms_id",
                    "front_cms_enable",
                    "front_cms_photo",
                    "front_cms_wifeMSG",
                    "front_cms_husMSG",
                    "front_cms_firstName"
            );
        sqlSession.close();
        return i;
    }

    public List<CustomMessageModel> getCustomMessagesForIndex() {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<CustomMessageModel> list=new ArrayList<>();
        Jedis jedis=new Jedis();
        try {
            List<String> front_cms_id = jedis.lrange("front_cms_id", 0, -1);
            if(front_cms_id.size()>0){
                for(int i=0;i<front_cms_id.size();i++){
                    CustomMessageModel customMessageModel = new CustomMessageModel();
                    customMessageModel.setEnable(Integer.valueOf(jedis.lrange("front_cms_enable",0,-1).get(i)));
                    customMessageModel.setId(Integer.valueOf(front_cms_id.get(i)));
                    customMessageModel.setPhoto(jedis.lrange("front_cms_photo",0,-1).get(i));
                    customMessageModel.setWifeMessage(jedis.lrange("front_cms_wifeMSG",0,-1).get(i));
                    customMessageModel.setHusMessage(jedis.lrange("front_cms_husMSG",0,-1).get(i));
                    customMessageModel.setFirstName(jedis.lrange("front_cms_firstName",0,-1).get(i));
                    list.add(customMessageModel);
                }

            }else {
               list= sqlSession.getMapper(CustomMessageDao.class).getCustomMessagesForIndex();
                for (CustomMessageModel customMessageModel : list) {
                    jedis.lpush("front_cms_enable",customMessageModel.getEnable()+"");
                    jedis.lpush("front_cms_id",customMessageModel.getId()+"");
                    jedis.lpush("front_cms_photo",customMessageModel.getPhoto()+"");
                    jedis.lpush("front_cms_wifeMSG",customMessageModel.getWifeMessage()+"");
                    jedis.lpush("front_cms_husMSG",customMessageModel.getHusMessage()+"");
                    jedis.lpush("front_cms_firstName",customMessageModel.getFirstName()+"");
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            jedis.close();
            sqlSession.close();
        }

        return list ;
    }
}
