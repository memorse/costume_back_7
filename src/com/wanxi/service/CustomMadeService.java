package com.wanxi.service;

import com.wanxi.dao.CustomeMadeDao;
import com.wanxi.daoImpl.CustomMadeDaoImpl;
import com.wanxi.model.CustomMadeModel;
import com.wanxi.test.MybatisTest;
import com.wanxi.util.JedisUtil;
import com.wanxi.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;

public class CustomMadeService implements CustomeMadeDao {

    @Override
    public List<CustomMadeModel> getCustomMades() {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<CustomMadeModel> customMades = sqlSession.getMapper(CustomeMadeDao.class).getCustomMades();
        sqlSession.close();
        return customMades;
    }

    @Override
    public List<CustomMadeModel> getCustomMadesByKey(String key) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<CustomMadeModel> customMadesByKey = sqlSession.getMapper(CustomeMadeDao.class).getCustomMadesByKey(key);
        sqlSession.close();
        return customMadesByKey;
    }

    public List<CustomMadeModel> getCustomMadesByKeyWithPage(String key,int startRows,int pageSize,int isPage) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<CustomMadeModel> customMadesByKeyWithPage = sqlSession.getMapper(CustomeMadeDao.class).getCustomMadesByKeyWithPage(key, startRows, pageSize, isPage);
        sqlSession.close();
        return customMadesByKeyWithPage;
    }

    @Override
    public int saveCustomMade(CustomMadeModel customMadeModel) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i= sqlSession.getMapper(CustomeMadeDao.class).saveCustomMade(customMadeModel);
        if(i>0)
            JedisUtil.delByKey(
                    "front_cmade_id",
                    "front_cmade_pubtime",
                    "front_cmade_detail",
                    "front_cmade_logo",
                    "front_cmade_name",
                    "front_cmade_order"
            );
        sqlSession.close();
        return i;
    }

    @Override
    public int updateCustomMade(CustomMadeModel customMadeModel) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i = sqlSession.getMapper(CustomeMadeDao.class).updateCustomMade(customMadeModel);
        if(i>0)
            JedisUtil.delByKey(
                    "front_cmade_id",
                    "front_cmade_pubtime",
                    "front_cmade_detail",
                    "front_cmade_logo",
                    "front_cmade_name",
                    "front_cmade_order"
            );
        sqlSession.close();
        return i;
    }

    @Override
    public int delCustomMade(int id) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i= sqlSession.getMapper(CustomeMadeDao.class).delCustomMade(id);
        if(i>0)
            JedisUtil.delByKey(
                    "front_cmade_id",
                    "front_cmade_pubtime",
                    "front_cmade_detail",
                    "front_cmade_logo",
                    "front_cmade_name",
                    "front_cmade_order"
            );
        sqlSession.close();
        return i;
    }

    public List<CustomMadeModel> getCustomMadesForIndex() {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<CustomMadeModel> list=new ArrayList<>();
        Jedis jedis=new Jedis();
        try {
            List<String> front_cmade_id = jedis.lrange("front_cmade_id", 0, -1);
            if(front_cmade_id.size()>0){
                for(int i=0;i<front_cmade_id.size();i++){
                    CustomMadeModel customMadeModel = new CustomMadeModel();
                    customMadeModel.setEnable(Integer.valueOf(jedis.lrange("front_cmade_enable",0,-1).get(i)));
                    customMadeModel.setStepOrder(Integer.valueOf(jedis.lrange("front_cmade_order",0,-1).get(i)));
                    customMadeModel.setId(Integer.valueOf(front_cmade_id.get(i)));
                    customMadeModel.setStepDetail(jedis.lrange("front_cmade_detail",0,-1).get(i));
                    customMadeModel.setStepLogo(jedis.lrange("front_cmade_logo",0,-1).get(i));
                    customMadeModel.setStepName(jedis.lrange("front_cmade_name",0,-1).get(i));
                    list.add(customMadeModel);
                }

            }else {

                list=sqlSession.getMapper(CustomeMadeDao.class).getCustomMadesForIndex();
                for (CustomMadeModel customMadeModel : list) {
                    jedis.lpush("front_cmade_detail",customMadeModel.getStepDetail());
                    jedis.lpush("front_cmade_logo",customMadeModel.getStepLogo());
                    jedis.lpush("front_cmade_name",customMadeModel.getStepName());
                    jedis.lpush("front_cmade_enable",customMadeModel.getEnable()+"");
                    jedis.lpush("front_cmade_id",customMadeModel.getId()+"");
                    jedis.lpush("front_cmade_order",customMadeModel.getStepOrder()+"");

                }

            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            jedis.close();
            sqlSession.close();
        }

        return list;
    }
}
