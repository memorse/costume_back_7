package com.wanxi.service;

import com.wanxi.dao.CompanyDao;
import com.wanxi.daoImpl.CompanyDaoImpl;
import com.wanxi.model.CompanyModel;
import com.wanxi.util.JedisUtil;
import com.wanxi.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import redis.clients.jedis.Jedis;

import java.util.List;

public class CompanyService implements CompanyDao {
    @Override
    public List<CompanyModel> getCompanys() {
        SqlSession sqlSession = MyBatisUtil.getSqlSession();
        List<CompanyModel> companys = sqlSession.getMapper(CompanyDao.class).getCompanys();
        sqlSession.close();
        return companys;
    }
    @Override
    public CompanyModel getCompanyForIndex() {
        CompanyModel companyModel=null;
        Jedis jedis = new Jedis();
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        String front_company_id = jedis.get("front_company_id");
        try {
            if(front_company_id!=null){
                companyModel = new CompanyModel();
                companyModel.setId(Integer.valueOf(jedis.get("front_company_id")));
                companyModel.setEnable(Integer.valueOf(jedis.get("front_company_enable")));
                companyModel.setAd(jedis.get("front_company_ad"));
                companyModel.setQq(jedis.get("front_company_qq"));
                companyModel.setPhone(jedis.get("front_company_phone"));
                companyModel.setAddress(jedis.get("front_company_address"));
                companyModel.setName(jedis.get("front_company_name"));
                companyModel.setIntroduce(jedis.get("front_company_introduce"));
                companyModel.setEmail(jedis.get("front_company_email"));
                companyModel.setLogo(jedis.get("front_company_logo"));
                companyModel.setQrCode(jedis.get("front_company_qrcode"));
            }else
            {
                companyModel = sqlSession.getMapper(CompanyDao.class).getCompanyForIndex();
                jedis.set("front_company_id",companyModel.getId()+"");
                jedis.set("front_company_enable",companyModel.getEnable()+"");
                jedis.set("front_company_phone",companyModel.getPhone());
                jedis.set("front_company_name",companyModel.getName());
                jedis.set("front_company_qq",companyModel.getQq());
                jedis.set("front_company_ad",companyModel.getAd());
                jedis.set("front_company_address",companyModel.getAddress());
                jedis.set("front_company_introduce",companyModel.getIntroduce());
                jedis.set("front_company_email",companyModel.getEmail());
                jedis.set("front_company_logo",companyModel.getLogo());
                jedis.set("front_company_qrcode",companyModel.getQrCode());
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            jedis.close();
            sqlSession.close();
        }

        return companyModel;
    }

    @Override
    public List<CompanyModel> getCompanysByKey(String key) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<CompanyModel> companysByKey = sqlSession.getMapper(CompanyDao.class).getCompanysByKey(key);
        sqlSession.close();
        return companysByKey;
    }

    public List<CompanyModel> getCompanysByKeyWithPage(String key,int pageSize,int currentPage) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        List<CompanyModel> companysByKeyWithPage = sqlSession.getMapper(CompanyDao.class).getCompanysByKeyWithPage(key, pageSize, currentPage);
        sqlSession.close();
        return companysByKeyWithPage;
    }

    @Override
    public int saveCompany(CompanyModel companyModel) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i= sqlSession.getMapper(CompanyDao.class).saveCompany(companyModel);
        if(i>0)
            JedisUtil.delByKey(
                    "front_company_id",
                    "front_company_enable",
                    "front_company_phone",
                    "front_company_name",
                    "front_company_qq",
                    "front_company_ad",
                    "front_company_address",
                    "front_company_introduce",
                    "front_company_email",
                    "front_company_logo",
                    "front_company_qrcode"
            );
        sqlSession.close();
        return i;
    }

    @Override
    public int delCompany(int id) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i= sqlSession.getMapper(CompanyDao.class).delCompany(id);
        if(i>0)
            JedisUtil.delByKey(
                    "front_company_id",
                    "front_company_enable",
                    "front_company_phone",
                    "front_company_name",
                    "front_company_qq",
                    "front_company_ad",
                    "front_company_address",
                    "front_company_introduce",
                    "front_company_email",
                    "front_company_logo",
                    "front_company_qrcode"
            );
        sqlSession.close();
        return i;
    }

    @Override
    public int updateCompany(CompanyModel companyModel) {
        SqlSession sqlSession= MyBatisUtil.getSqlSession();
        int i = sqlSession.getMapper(CompanyDao.class).updateCompany(companyModel);
        if(i>0){
            updateCompanyEnableToFalseExceptOne(companyModel.getId());
            JedisUtil.delByKey(
                    "front_company_id",
                    "front_company_enable",
                    "front_company_phone",
                    "front_company_name",
                    "front_company_qq",
                    "front_company_ad",
                    "front_company_address",
                    "front_company_introduce",
                    "front_company_email",
                    "front_company_logo",
                    "front_company_qrcode"
            );

        }

        sqlSession.close();
        return i;
    }

    @Override
    public int updateCompanyEnableToFalseExceptOne(int id) {
        SqlSession sqlSession = MyBatisUtil.getSqlSession();
        CompanyDao mapper = sqlSession.getMapper(CompanyDao.class);
        int i = mapper.updateCompanyEnableToFalseExceptOne(id);
        sqlSession.close();
        return i;
    }
}
