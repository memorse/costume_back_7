package com.wanxi.servlet.add;

import com.sun.org.apache.xpath.internal.operations.Mod;
import com.wanxi.model.CompanyModel;
import com.wanxi.service.CompanyService;
import com.wanxi.util.ModelUtil;
import com.wanxi.util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/addCompany")
public class AddCompanyServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        ServletUtil.setEncoding(req,resp);
        String company = req.getParameter("company");
        CompanyModel companyModel = ModelUtil.packagingCompanyFromString(company);
        int i = new CompanyService().saveCompany(companyModel);
        resp.getWriter().println(i);
    }
}
