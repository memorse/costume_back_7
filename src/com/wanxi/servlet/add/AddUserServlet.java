package com.wanxi.servlet.add;

import com.wanxi.model.UserModel;
import com.wanxi.service.UserService;
import com.wanxi.util.ServletUtil;
import com.wanxi.util.ModelUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/addUser")
public class AddUserServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);

        String user = req.getParameter("user");
        UserModel userModel = ModelUtil.packagingUserFromString(user);
//        JSONObject parse = (JSONObject) JSONObject.parse(user);
//        UserModel userModel = new UserModel();
//        userModel.setId(parse.getInteger("id"));
//        userModel.setAccount(parse.getString("user_account"));
//        userModel.setName(parse.getString("user_name"));
//        userModel.setNickName(parse.getString("user_nickname"));
//        userModel.setPhone(parse.getString("user_phone"));
//        userModel.setMate(parse.getString("user_mate"));
//        userModel.setGender(parse.getString("user_gender"));
//        userModel.setPassword(parse.getString("user_password"));
//        userModel.setHobby(parse.getString("user_hobby").split(","));
//        userModel.setBirthday(parse.getDate("user_birthday"));
//        userModel.setWeddingAnniversary(parse.getDate("user_wedding_anniversary"));
        int i = new UserService().saveUser(userModel);
        resp.getWriter().println(i);
    }
}
