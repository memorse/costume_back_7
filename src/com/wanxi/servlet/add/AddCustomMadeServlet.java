package com.wanxi.servlet.add;

import com.wanxi.model.CustomMadeModel;
import com.wanxi.service.CustomMadeService;
import com.wanxi.util.ModelUtil;
import com.wanxi.util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/addCmade")
public class AddCustomMadeServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);
        String cmade = req.getParameter("cmade");
        CustomMadeModel customMadeModel = ModelUtil.packagingCustomMadeFromString(cmade);
        int i = new CustomMadeService().saveCustomMade(customMadeModel);
        resp.getWriter().println(i);
    }
}
