package com.wanxi.servlet.add;

import com.wanxi.model.OfflineStoreModel;
import com.wanxi.service.OfflineStoreService;
import com.wanxi.util.ModelUtil;
import com.wanxi.util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/addOffStore")
public class AddOffStoreServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);
        String offStore = req.getParameter("offStore");
        OfflineStoreModel offlineStoreModel = ModelUtil.packagingOfflineStoreFromString(offStore);
        int i = new OfflineStoreService().savaOfflineStoreModel(offlineStoreModel);
        resp.getWriter().println(i);
    }
}
