package com.wanxi.servlet.add;

import com.wanxi.model.NewsSortModel;
import com.wanxi.service.NewsSortService;
import com.wanxi.util.ModelUtil;
import com.wanxi.util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/addNewsSort")
public class AddNewsSrotServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);
        String newsSort = req.getParameter("newsSort");
        NewsSortModel newsSortModel = ModelUtil.packagingNewsSortFromString(newsSort);
        int i = new NewsSortService().saveNewsSort(newsSortModel);
        resp.getWriter().println(i);
    }
}
