package com.wanxi.servlet.add;

import com.sun.org.apache.xpath.internal.operations.Mod;
import com.wanxi.model.ProductionModel;
import com.wanxi.service.ProductionService;
import com.wanxi.servlet.HomeServlet;
import com.wanxi.util.ModelUtil;
import com.wanxi.util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/addProduction")
public class AddProductionServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);
        String production = req.getParameter("production");
        ProductionModel productionModel = ModelUtil.packagingProductionFromString(production);
        int i = new ProductionService().saveProduction(productionModel);
        resp.getWriter().println(i);
    }
}
