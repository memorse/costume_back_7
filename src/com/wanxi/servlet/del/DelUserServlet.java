package com.wanxi.servlet.del;

import com.wanxi.service.UserService;
import com.wanxi.util.ServletUtil;
import org.junit.Test;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/delUser")
public class DelUserServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //解决乱码
        ServletUtil.setEncoding(req,resp);
        String id=req.getParameter("id");
        int i = new UserService().delUser(id);
        resp.getWriter().println(i);

    }
}
