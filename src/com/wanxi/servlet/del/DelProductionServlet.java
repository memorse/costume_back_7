package com.wanxi.servlet.del;

import com.wanxi.service.ProductionService;
import com.wanxi.servlet.HomeServlet;
import com.wanxi.util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/delProduction")
public class DelProductionServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);
        String id = req.getParameter("id");
        int i = new ProductionService().delProduction(Integer.valueOf(id));
        resp.getWriter().println(i);
    }
}
