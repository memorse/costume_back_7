package com.wanxi.servlet.del;

import com.wanxi.service.CompanyService;
import com.wanxi.util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/delCompany")
public class DelCompanyServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);
        String id=req.getParameter("id");
        int i = new CompanyService().delCompany(Integer.valueOf(id));
        resp.getWriter().println(i);

    }
}
