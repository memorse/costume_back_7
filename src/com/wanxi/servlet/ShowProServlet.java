package com.wanxi.servlet;

import com.alibaba.fastjson.JSONObject;
import com.wanxi.daoImpl.ProductionTypeDaoImpl;
import com.wanxi.model.*;
import com.wanxi.service.CompanyService;
import com.wanxi.service.NavigatorService;
import com.wanxi.service.NewsService;
import com.wanxi.service.ProductionService;
import com.wanxi.util.ServletUtil;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/production")
public class ShowProServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        ServletUtil.setEncoding(req,resp);
        //公司信息
        CompanyService companyService=new CompanyService();
        CompanyModel company = companyService.getCompanyForIndex();

        //导航信息
        NavigatorService navigatorService=new NavigatorService();
        List<NavigatorModel> navigators=navigatorService.getNavigatorsForIndex();


        //        新闻信息
        NewsService newsService=new NewsService();
        List<NewsModel> news= newsService.getNewsForIndex();


//        产品信息
        ProductionService productionService = new ProductionService();
        List<ProductionModel> productions = productionService.getProductionsForIndex();
        List<ProductionTypeModel> productionTypes = new ProductionTypeDaoImpl().getProductionTypes();

        JSONObject jsonObject=new JSONObject();
        jsonObject.put("company",company);
        jsonObject.put("navigator",navigators);
        jsonObject.put("news",news);
        jsonObject.put("productions",productions);
        jsonObject.put("productionTypes",productionTypes);
        resp.getWriter().println(jsonObject);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
