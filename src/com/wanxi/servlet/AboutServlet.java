package com.wanxi.servlet;

import com.alibaba.fastjson.JSONObject;
import com.sun.javaws.HtmlOptions;
import com.wanxi.model.*;
import com.wanxi.service.*;
import com.wanxi.util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/about")
public class AboutServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);
        //公司信息
        CompanyService companyService=new CompanyService();
        CompanyModel company = companyService.getCompanyForIndex();

        //导航信息
        NavigatorService navigatorService=new NavigatorService();
        List<NavigatorModel> navigators=navigatorService.getNavigatorsForIndex();

        //        新闻信息
        NewsService newsService=new NewsService();
        List<NewsModel> news= newsService.getNewsForIndex();

        //设计师
        DesignerService designerService = new DesignerService();
        List<DesignerModel> designers = designerService.getDesignersForIndex();

        OfflineStoreService offlineStoreService = new OfflineStoreService();
        List<OfflineStoreModel> offlineStores = offlineStoreService.getOfflineStoresForIndex();

        JSONObject jsonObject=new JSONObject();
        jsonObject.put("company",company);
        jsonObject.put("navigator",navigators);
        jsonObject.put("news",news);
        jsonObject.put("designers",designers);
        jsonObject.put("offlineStores",offlineStores);
        resp.getWriter().println(jsonObject);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
