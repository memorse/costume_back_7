package com.wanxi.servlet.update;

import com.wanxi.model.UserModel;
import com.wanxi.service.UserService;
import com.wanxi.util.ServletUtil;
import com.wanxi.util.ModelUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/updateUser")
public class UpdateUserServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);

        String user=req.getParameter("user");
        UserModel userModel = ModelUtil.packagingUserFromString(user);

        int i = new UserService().updateUser(userModel);
        resp.getWriter().println(i);
    }
}
