package com.wanxi.servlet.update;

import com.wanxi.model.CompanyModel;
import com.wanxi.service.CompanyService;
import com.wanxi.util.ModelUtil;
import com.wanxi.util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/updateCompany")
public class UpdateCompanyServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);

        String company = req.getParameter("company");
        CompanyModel companyModel = ModelUtil.packagingCompanyFromString(company);
        int i = new CompanyService().updateCompany(companyModel);
        resp.getWriter().println(i);
    }
}
