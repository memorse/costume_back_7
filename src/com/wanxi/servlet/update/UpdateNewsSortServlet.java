package com.wanxi.servlet.update;

import com.wanxi.model.NewsSortModel;
import com.wanxi.service.NewsSortService;
import com.wanxi.util.ModelUtil;
import com.wanxi.util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/updateNewsSort")
public class UpdateNewsSortServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);
        String newsSort = req.getParameter("newsSort");
        NewsSortModel newsSortModel = ModelUtil.packagingNewsSortFromString(newsSort);
        int i = new NewsSortService().updateNewsSort(newsSortModel);
        resp.getWriter().println(i);
    }
}
