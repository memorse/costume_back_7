package com.wanxi.servlet.update;

import com.wanxi.model.NewsModel;
import com.wanxi.service.NewsService;
import com.wanxi.util.ModelUtil;
import com.wanxi.util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/updateNews")
public class UpdateNewsServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);
        String news = req.getParameter("news");
        NewsModel newsModel = ModelUtil.packagingNewsFromString(news);
        int i = new NewsService().updateNews(newsModel);
        resp.getWriter().println(i);
    }
}
