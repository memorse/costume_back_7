package com.wanxi.servlet.update;

import com.wanxi.model.CustomMadeModel;
import com.wanxi.model.UserModel;
import com.wanxi.service.CustomMadeService;
import com.wanxi.util.ModelUtil;
import com.wanxi.util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/updateCmade")
public class UpdateCustomMadeServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);
        String cmade = req.getParameter("cmade");
        CustomMadeModel customMadeModel = ModelUtil.packagingCustomMadeFromString(cmade);
        int i = new CustomMadeService().updateCustomMade(customMadeModel);
        resp.getWriter().println(i);
    }
}
