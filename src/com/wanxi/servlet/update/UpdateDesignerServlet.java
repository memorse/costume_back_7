package com.wanxi.servlet.update;

import com.wanxi.model.DesignerModel;
import com.wanxi.service.DesignerService;
import com.wanxi.util.ModelUtil;
import com.wanxi.util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/updateDesigner")
public class UpdateDesignerServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);
        String designer = req.getParameter("designer");
        DesignerModel designerModel = ModelUtil.packagingDesignerFromString(designer);
        int i = new DesignerService().updateDesigner(designerModel);
        resp.getWriter().println(i);
    }
}
