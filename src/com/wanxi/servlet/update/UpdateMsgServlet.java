package com.wanxi.servlet.update;

import com.wanxi.model.CustomMessageModel;
import com.wanxi.service.CustomMessageService;
import com.wanxi.util.ModelUtil;
import com.wanxi.util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/updateMsg")
public class UpdateMsgServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);
        String message = req.getParameter("message");
        CustomMessageModel customMessageModel = ModelUtil.packagingCustomMessageFromString(message);
        int i = new CustomMessageService().updateCustomMessage(customMessageModel);
        resp.getWriter().println(i);
    }
}
