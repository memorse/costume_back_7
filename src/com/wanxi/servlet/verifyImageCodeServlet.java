package com.wanxi.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/verifyImageCode")
public class verifyImageCodeServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        String session_vcode=(String) req.getSession().getAttribute("text");    //从session中获取真正的验证码
        String code = req.getParameter("code");
        resp.getWriter().println(session_vcode.equals(code)?1:0);
    }
}
