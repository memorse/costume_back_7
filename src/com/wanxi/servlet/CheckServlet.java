package com.wanxi.servlet;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wanxi.model.UserModel;
import com.wanxi.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/checkAccount")
public class CheckServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String account=req.getParameter("account");
        UserModel userByUesrAccount = new UserService().getUserByUesrAccount(account);
        JSONObject jsonObject = new JSONObject();
        if(userByUesrAccount==null){
            jsonObject.put("status",1);//用户名可以用

        }else {
            jsonObject.put("status",0);//用户名存在，不可用
        }
        resp.getWriter().println(jsonObject);
    }
}
