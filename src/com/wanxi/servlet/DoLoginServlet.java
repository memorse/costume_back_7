package com.wanxi.servlet;

import com.alibaba.fastjson.JSONObject;
import com.wanxi.model.UserModel;
import com.wanxi.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/doLogin")
public class DoLoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        1.处理乱码
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
//        System.out.println("登录！");
//
//        2.获取参数
        String username=req.getParameter("username");
        String password=req.getParameter("password");

//        3.验证
        UserModel userModel = new UserService().verifyLogin(username, password);
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("status",userModel!=null?200:400);
        if(userModel!=null){
            jsonObject.put("account",userModel.getAccount());
            jsonObject.put("username",userModel.getNickName());
        }

        resp.getWriter().println(jsonObject);//200:验证通过，400验证失败
    }
}
