package com.wanxi.servlet;

import com.alibaba.fastjson.JSONObject;
import com.wanxi.model.CompanyModel;
import com.wanxi.model.CustomMessageModel;
import com.wanxi.model.NavigatorModel;
import com.wanxi.model.NewsModel;
import com.wanxi.service.CompanyService;
import com.wanxi.service.CustomMessageService;
import com.wanxi.service.NavigatorService;
import com.wanxi.service.NewsService;
import com.wanxi.util.ServletUtil;
import redis.clients.jedis.Jedis;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/home")
public class HomeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);
        String jsonpCallback = req.getParameter("callback");

            //公司信息
            CompanyService companyService=new CompanyService();
            CompanyModel company = companyService.getCompanyForIndex();


        //导航信息
        NavigatorService navigatorService=new NavigatorService();
        List<NavigatorModel> navigators=navigatorService.getNavigatorsForIndex();


        //        新闻信息
        NewsService newsService=new NewsService();
        List<NewsModel> news= newsService.getNewsForIndex();


        //        客户寄语
        CustomMessageService customMessageService=new CustomMessageService();
        List<CustomMessageModel> customMessageModels=customMessageService.getCustomMessagesForIndex();

        JSONObject jsonObject=new JSONObject();
        jsonObject.put("company",company);
        jsonObject.put("navigator",navigators);
        jsonObject.put("news",news);
        jsonObject.put("messages",customMessageModels);
        resp.getWriter().println(jsonObject);
//        resp.getWriter().println(jsonpCallback+"("+jsonObject.toJSONString()+")");

    }
}
