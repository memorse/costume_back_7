package com.wanxi.servlet.get;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wanxi.model.SearchDataModel;
import com.wanxi.model.UserModel;
import com.wanxi.service.UserService;
import com.wanxi.util.SQLUtil;
import com.wanxi.util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@WebServlet("/getUsersByKey")
public class GetUsersByKeyServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        ServletUtil.setEncoding(req,resp);
//        String key = req.getParameter("key");
//        key = key == null ? "" : key;
//        String page = req.getParameter("currentPage");
//        int currentPage = page == null || page.equals("")  ? 1 : Integer.valueOf(req.getParameter("currentPage"));
//        String unitPage = req.getParameter("unitPage");
//        int pageSize = unitPage==null||unitPage==""?10:Integer.valueOf(unitPage);
//        int count=new UserService().getUsersByKey(key).size();
//        int startPage = (currentPage - 1) * pageSize;//查询的起始记录
//        List<UserModel> users = new UserService().getUsersByKeyWithPage(key,startPage,pageSize);
//        int sumOfPage = count % pageSize == 0 ? count / pageSize : count / pageSize + 1;//总页数
//        ServletUtil.writeModelToJSON(resp,users,currentPage,sumOfPage,pageSize);

        ServletUtil.setEncoding(req,resp);
        String datas=req.getParameter("searchData");
        JSONObject searchDatas = (JSONObject) JSONObject.parse(datas);

        String key = searchDatas.getString("key")==null?"":(String)searchDatas.get("key");

        String page = req.getParameter("currentPage");
        int currentPage = page == null || page.equals("")  ? 1 : Integer.valueOf(req.getParameter("currentPage"));

        String getPageSize = searchDatas.getString("pageSize");
        int pageSize = getPageSize==null||getPageSize.equals("")?10:Integer.valueOf(getPageSize);


        String gender= searchDatas.getString("gender");
        String[] hobbys=searchDatas.getString("hobby").split(",");

        Date beginDate=searchDatas.getDate("begin_date");
        Date endDate=searchDatas.getDate("end_date");
        int startPage = (currentPage - 1) * pageSize;//查询的起始记录
        SearchDataModel searchDataModel = new SearchDataModel();
        searchDataModel.setKey(key);
        searchDataModel.setBeginDate(beginDate);
        searchDataModel.setEndDate(endDate);
        searchDataModel.setCurrentPage(currentPage);
        searchDataModel.setPageSize(pageSize);
        searchDataModel.setStartPage(startPage);
        searchDataModel.setGender(gender);

        List<String> myhobby=new ArrayList<>();
        for (String hobby : hobbys) {
            myhobby.add(hobby);
        }
        searchDataModel.setHobbys(myhobby);
        int count=new UserService().getUsersByKeyWithPagePlus(searchDataModel,0).size();
        List<UserModel> users = new UserService().getUsersByKeyWithPagePlus(searchDataModel,1);
        int sumOfPage = count % pageSize == 0 ? count / pageSize : count / pageSize + 1;//总页数
        ServletUtil.writeModelToJSON(resp,users,currentPage,sumOfPage,pageSize);

    }
}
