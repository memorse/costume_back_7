package com.wanxi.servlet.get;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wanxi.model.CompanyModel;
import com.wanxi.service.CompanyService;
import com.wanxi.util.ServletUtil;
import org.junit.Test;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/getCompany")
public class GetCompanyServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);

        List<CompanyModel> companys = new CompanyService().getCompanys();
        JSONObject jsonObject=new JSONObject();
        ServletUtil.writeModelToJSON(resp,jsonObject,"companys",companys);
    }
}
