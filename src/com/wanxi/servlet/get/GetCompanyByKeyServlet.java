package com.wanxi.servlet.get;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wanxi.model.CompanyModel;
import com.wanxi.service.CompanyService;
import com.wanxi.util.SQLUtil;
import com.wanxi.util.ServletUtil;
import redis.clients.jedis.Jedis;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/getCompanyByKey")
public class GetCompanyByKeyServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req, resp);
        String key = req.getParameter("key");
        key = key == null ? "" : key;

        String page = req.getParameter("currentPage");
        int currentPage = page == null || page.equals("")  ? 1 : Integer.valueOf(req.getParameter("currentPage"));

        String unitPage = req.getParameter("unitPage");

        int pageSize = unitPage==null||unitPage==""?10:Integer.valueOf(unitPage);

        int count=new CompanyService().getCompanysByKey(key).size();

        int startPage = (currentPage - 1) * pageSize;//查询的起始记录

        int sumOfPage = count % pageSize == 0 ? count / pageSize : count / pageSize + 1;//总页数

        JSONObject jsonObject = new JSONObject();
        List<CompanyModel> companysByKey = new CompanyService().getCompanysByKeyWithPage(key, pageSize, startPage);
        if (companysByKey.size() > 0) {
            jsonObject.put("status", 200);
            jsonObject.put("list", companysByKey);
            jsonObject.put("currentPage", currentPage);
            jsonObject.put("count", sumOfPage);
            jsonObject.put("pageSize", pageSize);
        } else {
            jsonObject.put("status", 400);
        }
        resp.getWriter().println(jsonObject.toJSONString());


    }
}
