package com.wanxi.servlet.get;

import com.alibaba.fastjson.JSONObject;
import com.wanxi.model.OfflineStoreModel;
import com.wanxi.service.OfflineStoreService;
import com.wanxi.util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/getOffStores")
public class GetOffStoreServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);
        List<OfflineStoreModel> offlineStores = new OfflineStoreService().getOfflineStores();
        JSONObject jsonObject=new JSONObject();
        ServletUtil.writeModelToJSON(resp,jsonObject,"offStores",offlineStores);
    }
}
