package com.wanxi.servlet.get;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wanxi.model.CustomMessageModel;
import com.wanxi.service.CustomMessageService;
import com.wanxi.util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/getMessages")
public class GetMsgsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);
        List<CustomMessageModel> customMessages = new CustomMessageService().getCustomMessages();
        JSONObject jsonObject=new JSONObject();
        if(customMessages.size()>0){
            jsonObject.put("status",200);
            jsonObject.put("messages", JSONArray.parseArray(JSONObject.toJSONString(customMessages)));
        }else {
            jsonObject.put("status",400);
        }
        resp.getWriter().println(jsonObject);
    }
}
