package com.wanxi.servlet.get;

import com.alibaba.fastjson.JSONObject;
import com.wanxi.model.NewsSortModel;
import com.wanxi.service.NewsSortService;
import com.wanxi.util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/getNewsSorts")
public class GetNewsSortsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);
        List<NewsSortModel> newsSorts = new NewsSortService().getNewsSorts();
        JSONObject jsonObject=new JSONObject();
        ServletUtil.writeModelToJSON(resp,jsonObject,"newsSortList",newsSorts);
    }
}
