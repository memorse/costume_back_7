package com.wanxi.servlet.get;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wanxi.model.CustomMadeModel;
import com.wanxi.service.CustomMadeService;
import com.wanxi.util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/getCustomMades")
public class GetCustomMadesServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);
        List<CustomMadeModel> customMadeSteps = new CustomMadeService().getCustomMades();

        JSONObject jsonObject=new JSONObject();
        if(customMadeSteps.size()>0){
            JSONArray jsonArray = JSONArray.parseArray(JSONObject.toJSONString(customMadeSteps));
            jsonObject.put("list",jsonArray);
            jsonObject.put("status",200);
        }else {
            jsonObject.put("status",400);
        }
        resp.getWriter().println(jsonObject);
    }
}
