package com.wanxi.servlet.get;

import com.alibaba.fastjson.JSONObject;
import com.wanxi.model.CustomMessageModel;
import com.wanxi.model.DesignerLevelModel;
import com.wanxi.model.DesignerModel;
import com.wanxi.service.CustomMessageService;
import com.wanxi.service.DesignerLevelService;
import com.wanxi.service.DesignerService;
import com.wanxi.util.SQLUtil;
import com.wanxi.util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/getDisignersByKey")
public class GetDesignersByKeyServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);
        String key = req.getParameter("key");
        key = key == null ? "" : key;
        String page = req.getParameter("currentPage");
        int currentPage = page == null || page.equals("")  ? 1 : Integer.valueOf(req.getParameter("currentPage"));
        String unitPage = req.getParameter("unitPage");
        int pageSize = unitPage==null||unitPage==""?10:Integer.valueOf(unitPage);

        int startPage = (currentPage - 1) * pageSize;//查询的起始记录
        int count=new DesignerService().getDesignersByKeyWithPage(key,startPage,pageSize,0).size();
        List<DesignerModel> designers = new DesignerService().getDesignersByKeyWithPage(key,startPage,pageSize,1);
        List<DesignerLevelModel> designerLevels = new DesignerLevelService().getDesignerLevels();
        int sumOfPage = count % pageSize == 0 ? count / pageSize : count / pageSize + 1;//总页数
        ServletUtil.writeModelToJSON(resp,designers,designerLevels,currentPage,sumOfPage,pageSize);
    }
}
