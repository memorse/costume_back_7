package com.wanxi.servlet.get;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wanxi.model.CustomMadeModel;
import com.wanxi.service.CustomMadeService;
import com.wanxi.util.ModelUtil;
import com.wanxi.util.SQLUtil;
import com.wanxi.util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/getCmadeByKey")
public class GetCustomMadeByKeyServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);
        String key = req.getParameter("key");
        key = key == null ? "" : key;
        String page = req.getParameter("currentPage");
        int currentPage = page == null || page.equals("")  ? 1 : Integer.valueOf(req.getParameter("currentPage"));
        String unitPage = req.getParameter("unitPage");
        int pageSize = unitPage==null||unitPage==""?10:Integer.valueOf(unitPage);
        int startPage = (currentPage - 1) * pageSize;//查询的起始记录
        int count=new CustomMadeService().getCustomMadesByKeyWithPage(key,startPage,pageSize,1).size();
        List<CustomMadeModel> customMadesByKey = new CustomMadeService().getCustomMadesByKeyWithPage(key,startPage,pageSize,1);
        int sumOfPage = count % pageSize == 0 ? count / pageSize : count / pageSize + 1;//总页数
        ServletUtil.writeModelToJSON(resp,customMadesByKey,currentPage,sumOfPage,pageSize);
    }
}
