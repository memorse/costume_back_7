package com.wanxi.servlet.get;

import com.alibaba.fastjson.JSONObject;
import com.wanxi.model.DesignerLevelModel;
import com.wanxi.model.DesignerModel;
import com.wanxi.service.DesignerLevelService;
import com.wanxi.service.DesignerService;
import com.wanxi.util.ServletUtil;

import javax.print.DocFlavor;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/getDesigners")
public class GetDesignersServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);
        List<DesignerModel> designers = new DesignerService().getDesigners();
        List<DesignerLevelModel> designerLevels = new DesignerLevelService().getDesignerLevels();
        JSONObject jsonObject=new JSONObject();
        if(designers.size()>0){
            jsonObject.put("designers",designers);
            jsonObject.put("designerLevels",designerLevels);
            jsonObject.put("status",200);
        }else {
            jsonObject.put("status",400);
        }
        resp.getWriter().println(jsonObject);

    }
}
