package com.wanxi.servlet.get;

import com.alibaba.fastjson.JSONObject;
import com.wanxi.model.NewsModel;
import com.wanxi.model.NewsSortModel;
import com.wanxi.service.NewsService;
import com.wanxi.service.NewsSortService;
import com.wanxi.util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/getNews")
public class GetNewsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);
        List<NewsModel> news = new NewsService().getNews();
        JSONObject jsonObject=new JSONObject();
        List<NewsSortModel> newsSorts = new NewsSortService().getNewsSorts();
        if(news.size()>0){
            jsonObject.put("status",200);
            jsonObject.put("newsList",news);
            jsonObject.put("newsSort",newsSorts);
        }else {
            jsonObject.put("status",400);
        }
        resp.getWriter().println(jsonObject);
    }
}
