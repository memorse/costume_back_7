package com.wanxi.servlet.get;

import com.alibaba.fastjson.JSONObject;
import com.wanxi.model.DesignerModel;
import com.wanxi.model.ProductionModel;
import com.wanxi.service.DesignerService;
import com.wanxi.service.ProductionService;
import com.wanxi.servlet.HomeServlet;
import com.wanxi.util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/getProductions")
public class GetProductionsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);
        List<ProductionModel> productions = new ProductionService().getProductions();
        List<DesignerModel> designers = new DesignerService().getDesigners();
        JSONObject jsonObject=new JSONObject();
        if(productions.size()>0){
            jsonObject.put("status",200);
            jsonObject.put("productions",productions);
            jsonObject.put("designers",designers);
        }else {
            jsonObject.put("status",400);
        }
        resp.getWriter().println(jsonObject);
    }
}
