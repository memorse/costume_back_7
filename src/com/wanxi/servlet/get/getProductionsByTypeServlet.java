package com.wanxi.servlet.get;

import com.alibaba.fastjson.JSONObject;
import com.wanxi.daoImpl.ProductionDaoImpl;
import com.wanxi.model.ProductionModel;
import com.wanxi.service.ProductionService;
import com.wanxi.util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/getProductionByType")
public class getProductionsByTypeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);
        String pro_type = req.getParameter("pro_type");
        List<ProductionModel> productions = new ProductionService().getProductionsByType(Integer.valueOf(pro_type));
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("productions",productions);
        resp.getWriter().println(jsonObject);
    }
}
