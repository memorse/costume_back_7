package com.wanxi.servlet.get;

import com.alibaba.fastjson.JSONObject;
import com.wanxi.model.NavigatorModel;
import com.wanxi.service.NavigatorService;
import com.wanxi.util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/getNavigators")
public class GetNavigatorsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletUtil.setEncoding(req,resp);
        List<NavigatorModel> navigators = new NavigatorService().getNavigators();
        JSONObject jsonObject=new JSONObject();
        ServletUtil.writeModelToJSON(resp,jsonObject,"navigators",navigators);
    }
}
