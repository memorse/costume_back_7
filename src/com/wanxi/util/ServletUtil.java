package com.wanxi.util;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.sun.deploy.net.HttpResponse;
import com.wanxi.model.DesignerLevelModel;
import com.wanxi.model.DesignerModel;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class ServletUtil {
    public static void setEncoding(HttpServletRequest req, HttpServletResponse resp){
        try {
            resp.setHeader("Content-Type", "application/json;charset=utf-8");
            req.setCharacterEncoding("UTF-8");
            resp.setCharacterEncoding("UTF-8");
            resp.setHeader("Access-Control-Allow-Origin","*");
            /*星号表示所有的异域请求都可以接受，*/
            resp.setHeader("Access-Control-Allow-Methods","GET,POST");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据list集合封装JSON数组并返回到页面
     * @param resp HttpServletResponse 对象
     * @param jsonName 返回的JSON 名  name : value  中的value
     * @param models list集合
     * @throws IOException
     */
    public static void writeModelToJSON(HttpServletResponse resp,JSONObject jsonObject,String jsonName, List models) throws IOException {
        if(models.size()>0){
            jsonObject.put("status",200);
            jsonObject.put(jsonName,models);
        }else {
            jsonObject.put("status",400);
        }

        resp.getWriter().println(jsonObject);
    }
    public static void writeModelToJSON(HttpServletResponse resp,List models,int currentPage,int sumOfPage,int pageSize) throws IOException {
        JSONObject jsonObject = new JSONObject();
        if (models.size() > 0) {
            jsonObject.put("status", 200);
            jsonObject.put("list", models);
            jsonObject.put("currentPage", currentPage);
            jsonObject.put("count", sumOfPage);
            jsonObject.put("pageSize", pageSize);
        } else {
            jsonObject.put("status", 400);
        }
        resp.getWriter().println(jsonObject);
    }

    public static void writeModelToJSON(HttpServletResponse resp,List models,List child,int currentPage,int sumOfPage,int pageSize) throws IOException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.DEFFAULT_DATE_FORMAT="yyyy-MM-dd:hh:mm:ss";
        if (models.size() > 0) {
            jsonObject.put("status", 200);
            jsonObject.put("list", models);
            jsonObject.put("child_list", child);
            jsonObject.put("currentPage", currentPage);
            jsonObject.put("count", sumOfPage);
            jsonObject.put("pageSize", pageSize);
        } else {
            jsonObject.put("status", 400);
        }
        resp.getWriter().println(jsonObject);
    }

    public static void writeModelToJSON(HttpServletResponse resp,List models,List child1,List child2,int currentPage,int sumOfPage,int pageSize) throws IOException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.DEFFAULT_DATE_FORMAT="yyyy-MM-dd:hh:mm:ss";
        if (models.size() > 0) {
            jsonObject.put("status", 200);
            jsonObject.put("list", models);
            jsonObject.put("child_list", child1);
            jsonObject.put("child_list2", child2);
            jsonObject.put("currentPage", currentPage);
            jsonObject.put("count", sumOfPage);
            jsonObject.put("pageSize", pageSize);
        } else {
            jsonObject.put("status", 400);
        }
        resp.getWriter().println(jsonObject);
    }
}
