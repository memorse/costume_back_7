package com.wanxi.util;

import com.alibaba.fastjson.JSONObject;
import com.wanxi.daoImpl.ProductionTypeDaoImpl;
import com.wanxi.model.*;
import com.wanxi.service.DesignerLevelService;
import com.wanxi.service.DesignerService;
import com.wanxi.service.NewsSortService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ModelUtil {
    /**
     * 将JSON字符串转化为User对象
     *
     * @param userStr Json字符串
     * @return UserModel
     */
    public static UserModel packagingUserFromString(String userStr) {
        JSONObject parse = (JSONObject) JSONObject.parse(userStr);
        UserModel userModel = new UserModel();
        if (!parse.getString("id").equals("")) {
            userModel.setId(parse.getInteger("id"));
        }
        userModel.setAccount(parse.getString("user_account"));
        userModel.setPassword(parse.getString("user_password"));
        userModel.setName(parse.getString("user_name"));
        userModel.setNickName(parse.getString("user_nickname"));
        userModel.setPhone(parse.getString("user_phone"));
        userModel.setMate(parse.getString("user_mate"));
        userModel.setGender(parse.getString("user_gender").equals("男")?"1":"0");
        userModel.setPassword(parse.getString("user_password"));
        userModel.setHobby(parse.getString("user_hobby"));
        userModel.setBirthday(parse.getDate("user_birthday"));
        userModel.setWeddingAnniversary(parse.getDate("user_wedding_anniversary"));
        userModel.setEnable(parse.getIntValue("enable"));
        userModel.setIsAdmin(parse.getIntValue("is_admin"));
        return userModel;
    }

    public static CompanyModel packagingCompanyFromString(String userStr) {
        JSONObject parse = (JSONObject) JSONObject.parse(userStr);
        CompanyModel companyModel = new CompanyModel();
        if (!parse.getString("id").equals("")) {
            companyModel.setId(parse.getInteger("id"));
        }
        companyModel.setEmail(parse.getString("email"));
        companyModel.setIntroduce(parse.getString("introduce"));
        companyModel.setAddress(parse.getString("address"));
        companyModel.setAd(parse.getString("ad"));
        companyModel.setPhone(parse.getString("phone"));
        companyModel.setName(parse.getString("name"));
        companyModel.setQq(parse.getString("qq"));
        companyModel.setLogo(parse.getString("logo"));
        companyModel.setQrCode(parse.getString("qrcode"));
        companyModel.setEnable(parse.getIntValue("enable"));
        return companyModel;
    }

    public static CustomMadeModel packagingCustomMadeFromString(String cmadeStr) {
        JSONObject parse = (JSONObject) JSONObject.parse(cmadeStr);
        CustomMadeModel customMadeModel=new CustomMadeModel();
        if(!parse.getString("id").equals("")){
            customMadeModel.setId(Integer.valueOf(parse.getString("id")));
        }
        customMadeModel.setStepName(parse.getString("name"));
        customMadeModel.setStepLogo(parse.getString("logo"));
        customMadeModel.setStepDetail(parse.getString("detail"));
        customMadeModel.setStepOrder(parse.getIntValue("order"));
        customMadeModel.setEnable(parse.getIntValue("enable"));

        return customMadeModel;
    }

    public static CustomMessageModel packagingCustomMessageFromString(String cmadeStr) {
        JSONObject parse = (JSONObject) JSONObject.parse(cmadeStr);
        CustomMessageModel customMessageModel=new CustomMessageModel();
        if(!parse.getString("id").equals("")){
            customMessageModel.setId(Integer.valueOf(parse.getString("id")));
        }
        customMessageModel.setFirstName(parse.getString("firstName"));
        customMessageModel.setHusMessage(parse.getString("husMsg"));
        customMessageModel.setWifeMessage(parse.getString("wifeMsg"));
        customMessageModel.setPhoto(parse.getString("photo"));
        customMessageModel.setEnable(parse.getIntValue("enable"));
        return customMessageModel;
    }

    public static DesignerModel packagingDesignerFromString(String cmadeStr) {
        JSONObject parse = (JSONObject) JSONObject.parse(cmadeStr);
        DesignerModel designerModel=new DesignerModel();
        if(!parse.getString("id").equals("")){
            designerModel.setId(Integer.valueOf(parse.getString("id")));
        }
        designerModel.setName(parse.getString("name"));
        designerModel.setGender(parse.getString("gender").equals("男")?"1":"0");
        designerModel.setPhoto(parse.getString("photo"));
        designerModel.setIntroduce(parse.getString("introduce"));
        designerModel.setLevel(new DesignerLevelService().getDesignerLevelById(parse.getIntValue("level")));
        designerModel.setEnable(parse.getIntValue("enable"));
        return designerModel;
    }

    public static NewsModel packagingNewsFromString(String cmadeStr) {
        JSONObject parse = (JSONObject) JSONObject.parse(cmadeStr);
        NewsModel newsModel=new NewsModel();
        if(!parse.getString("id").equals("")){
            newsModel.setId(Integer.valueOf(parse.getString("id")));
        }
        newsModel.setTitle(parse.getString("title"));
        newsModel.setContent(parse.getString("content"));
        newsModel.setImgRoute(parse.getString("imgRoute"));
        newsModel.setPubTime(parse.getDate("pubTime"));
        newsModel.setEnable(parse.getIntValue("enable"));
        newsModel.setNewsSort(new NewsSortService().getNewsSortsById(parse.getIntValue("news_sort")));
        return newsModel;
    }

    public static NewsSortModel packagingNewsSortFromString(String cmadeStr) {
        JSONObject parse = (JSONObject) JSONObject.parse(cmadeStr);
        NewsSortModel newsSortModel=new NewsSortModel();
        if(!parse.getString("id").equals("")){
            newsSortModel.setId(Integer.valueOf(parse.getString("id")));
        }
        newsSortModel.setType(parse.getString("type"));
        newsSortModel.setPriKey(parse.getString("prikey"));
        newsSortModel.setEnable(parse.getIntValue("enable"));
        return newsSortModel;
    }

    public static NavigatorModel packagingNavigatorFromString(String cmadeStr) {
        JSONObject parse = (JSONObject) JSONObject.parse(cmadeStr);
        NavigatorModel navigatorModel=new NavigatorModel();
        if(!parse.getString("id").equals("")){
            navigatorModel.setId(Integer.valueOf(parse.getString("id")));
        }
        navigatorModel.setName(parse.getString("name"));
        navigatorModel.setTarget(parse.getString("target"));
        navigatorModel.setLevel(parse.getIntValue("level"));
        navigatorModel.setParent(parse.getIntValue("parent"));
        navigatorModel.setEnable(parse.getIntValue("enable"));
        return navigatorModel;
    }

    public static ProductionModel packagingProductionFromString(String cmadeStr) {
        JSONObject parse = (JSONObject) JSONObject.parse(cmadeStr);
        ProductionModel productionModel=new ProductionModel();
        if(!parse.getString("id").equals("")){
            productionModel.setId(Integer.valueOf(parse.getString("id")));
        }
        productionModel.setProName(parse.getString("name"));
        productionModel.setProPrice(parse.getString("price"));
        productionModel.setProPhoto(parse.getString("photo"));
        productionModel.setDesigner(new DesignerService().getDesignersById(parse.getIntValue("designer")));
        productionModel.setEnable(parse.getIntValue("enable"));
        productionModel.setType(new ProductionTypeDaoImpl().getProductionTypeById(parse.getIntValue("type")));
        return productionModel;
    }

    public static ProductionTypeModel packagingProductionTypeFromString(String cmadeStr) {
        JSONObject parse = (JSONObject) JSONObject.parse(cmadeStr);
        ProductionTypeModel productionTypeModel=new ProductionTypeModel();
        if(!parse.getString("id").equals("")){
            productionTypeModel.setId(Integer.valueOf(parse.getString("id")));
        }
        productionTypeModel.setName(parse.getString("name"));
        return productionTypeModel;
    }

    public static OfflineStoreModel packagingOfflineStoreFromString(String cmadeStr) {
        JSONObject parse = (JSONObject) JSONObject.parse(cmadeStr);
        OfflineStoreModel offlineStoreModel=new OfflineStoreModel();
        if(!parse.getString("id").equals("")){
            offlineStoreModel.setId(Integer.valueOf(parse.getString("id")));
        }
        offlineStoreModel.setName(parse.getString("name"));
        offlineStoreModel.setPhoto(parse.getString("photo"));
        offlineStoreModel.setAddress(parse.getString("address"));
        offlineStoreModel.setPhone(parse.getString("phone"));
        offlineStoreModel.setEnable(parse.getIntValue("enable"));


        return offlineStoreModel;
    }

    public static DesignerLevelModel packagingDesignerLevelFromString(String cmadeStr) {
        JSONObject parse = (JSONObject) JSONObject.parse(cmadeStr);
        DesignerLevelModel designerLevelModel=new DesignerLevelModel();
        if(!parse.getString("id").equals("")){
            designerLevelModel.setId(Integer.valueOf(parse.getString("id")));
        }
        designerLevelModel.setLevelName(parse.getString("level_name"));
        return designerLevelModel;
    }




    public static List<UserModel> getListUserModelFromResultSet(ResultSet resultSet) throws SQLException {
        List<UserModel> userModels = new ArrayList<>();
        while (resultSet.next()) {
            UserModel userModel=new UserModel();
            userModel.setId(resultSet.getInt("id"));
            userModel.setAccount(resultSet.getString("user_account"));
            userModel.setHobby(resultSet.getString("user_hobby"));
            userModel.setGender(resultSet.getInt("user_gender")==1?"男":"女");
            userModel.setMate(resultSet.getString("user_mate"));
            userModel.setName(resultSet.getString("user_name"));
            userModel.setNickName(resultSet.getString("user_nickname"));
            userModel.setBirthday(resultSet.getDate("user_birthday"));
            userModel.setPhone(resultSet.getString("user_phone"));
            userModel.setPassword(resultSet.getString("user_pwd"));
            userModel.setWeddingAnniversary(resultSet.getDate("user_wedding_anniversary"));
            userModel.setIsAdmin(resultSet.getInt("user_is_admin"));
            userModel.setEnable(resultSet.getInt("enable"));
            userModels.add(userModel);
        }
        return userModels;
    }

    public static List<CompanyModel> getListCompanyFromResultSet(ResultSet resultSet) throws SQLException {
        List<CompanyModel> companyModels = new ArrayList<>();
        while (resultSet.next()) {
            CompanyModel companyModel = new CompanyModel();
            companyModel.setId(resultSet.getInt("id"));
            companyModel.setName(resultSet.getString("c_name"));
            companyModel.setAd(resultSet.getString("c_ad"));
            companyModel.setQq(resultSet.getString("c_qq"));
            companyModel.setPhone(resultSet.getString("c_phone"));
            companyModel.setAddress(resultSet.getString("c_address"));
            companyModel.setIntroduce(resultSet.getString("c_introduce"));
            companyModel.setEmail(resultSet.getString("c_email"));
            companyModel.setQrCode(resultSet.getString("c_qrcode"));
            companyModel.setLogo(resultSet.getString("c_logo"));
            companyModel.setEnable(resultSet.getInt("enable"));
            companyModels.add(companyModel);
        }
        return companyModels;
    }

    public static List<CustomMadeModel> getListCustomMadeFromResultSet(ResultSet resultSet) throws SQLException {
        List<CustomMadeModel> CustomMadeModels = new ArrayList<>();
        while(resultSet.next()){
            CustomMadeModel customMadeModel=new CustomMadeModel();
            customMadeModel.setId(resultSet.getInt("id"));
            customMadeModel.setStepOrder(resultSet.getInt("cmade_step_order"));
            customMadeModel.setStepDetail(resultSet.getString("cmade_step_detail"));
            customMadeModel.setStepLogo(resultSet.getString("cmade_step_logo"));
            customMadeModel.setStepName(resultSet.getString("cmade_step_name"));
            customMadeModel.setEnable(resultSet.getInt("enable"));
            CustomMadeModels.add(customMadeModel);
        }
        return CustomMadeModels;
    }

    public static List<CustomMessageModel> getListCustomMessageFromResultSet(ResultSet resultSet) throws SQLException {
        List<CustomMessageModel> customMessageModels = new ArrayList<>();
        while (resultSet.next()){
            CustomMessageModel customMessageModel=new CustomMessageModel();
            customMessageModel.setId(resultSet.getInt("id"));
            customMessageModel.setFirstName(resultSet.getString("cmsg_firstname"));
            customMessageModel.setWifeMessage(resultSet.getString("cmsg_wife_msg"));
            customMessageModel.setHusMessage(resultSet.getString("cmsg_hus_msg"));
            customMessageModel.setPhoto(resultSet.getString("cmsg_photo"));
            customMessageModel.setEnable(resultSet.getInt("enable"));
            customMessageModels.add(customMessageModel);
        }
        return customMessageModels;
    }

    public static List<DesignerModel> getListDesignerModelFromResultSet(ResultSet resultSet) throws SQLException {
        List<DesignerModel> designerModels = new ArrayList<>();
        while (resultSet.next()){
            DesignerModel designerModel = new DesignerModel();
            designerModel.setId(resultSet.getInt("id"));
            designerModel.setPhoto(resultSet.getString("designer_photo"));
            designerModel.setIntroduce(resultSet.getString("designer_introduce"));
            designerModel.setGender(resultSet.getInt("designer_gender")==1?"男":"女");
            designerModel.setName(resultSet.getString("designer_name"));
            designerModel.setEnable(resultSet.getInt("enable"));
            designerModel.setLevel(new DesignerLevelService().getDesignerLevelById(resultSet.getInt("designer_level")));
            designerModels.add(designerModel);
        }
        return designerModels;
    }

    public static List<NewsModel> getListNewsModelFromResultSet(ResultSet resultSet) throws SQLException {
        List<NewsModel> newsModels = new ArrayList<>();
        while (resultSet.next()){
            NewsModel newsModel=new NewsModel();
            newsModel.setId(resultSet.getInt("id"));
            newsModel.setImgRoute(resultSet.getString("news_img_route"));
            newsModel.setContent(resultSet.getString("news_content"));
            newsModel.setTitle(resultSet.getString("news_title"));
            newsModel.setPubTime(resultSet.getDate("news_pubtime"));
            newsModel.setEnable(resultSet.getInt("enable"));
            newsModel.setNewsSort(new NewsSortService().getNewsSortsById(resultSet.getInt("news_sort")));
            newsModels.add(newsModel);
        }
        return newsModels;
    }

    public static List<NewsSortModel> getListNewsSortModelFromResultSet(ResultSet resultSet) throws SQLException {
        List<NewsSortModel> newsSortModels = new ArrayList<>();
        while (resultSet.next()){
            NewsSortModel newsSortModel = new NewsSortModel();
            newsSortModel.setId(resultSet.getInt("id"));
            newsSortModel.setType(resultSet.getString("news_sort_type"));
            newsSortModel.setPriKey(resultSet.getString("news_sort_prikey"));
            newsSortModel.setEnable(resultSet.getInt("enable"));
            newsSortModels.add(newsSortModel);
        }
        return newsSortModels;
    }

    public static List<NavigatorModel> getListNavigatorModelFromResultSet(ResultSet resultSet) throws SQLException {
        List<NavigatorModel> navigatorModels = new ArrayList<>();
        while (resultSet.next()){
            NavigatorModel navigatorModel=new NavigatorModel();
            navigatorModel.setId(resultSet.getInt("id"));
            navigatorModel.setLevel(resultSet.getInt("nav_level"));
            navigatorModel.setTarget(resultSet.getString("nav_target"));
            navigatorModel.setName(resultSet.getString("nav_name"));
            navigatorModel.setParent(resultSet.getInt("nav_parent"));
            navigatorModel.setEnable(resultSet.getInt("enable"));
            navigatorModels.add(navigatorModel);
        }
        return navigatorModels;
    }

    public static List<ProductionModel> getListProductionModelFromResultSet(ResultSet resultSet) throws SQLException {
        List<ProductionModel> productionModels = new ArrayList<>();
        while (resultSet.next()){
            ProductionModel productionModel = new ProductionModel();
            productionModel.setId(resultSet.getInt("id"));
            productionModel.setProName(resultSet.getString("pro_name"));
            productionModel.setProPhoto(resultSet.getString("pro_photo"));
            productionModel.setProPrice(resultSet.getString("pro_price"));
            productionModel.setEnable(resultSet.getInt("enable"));
            int pro_type = resultSet.getInt("pro_type");
            productionModel.setDesigner(new DesignerService().getDesignersById(resultSet.getInt("pro_designer")));
            productionModel.setType(new ProductionTypeDaoImpl().getProductionTypeById(pro_type));
            productionModels.add(productionModel);
        }
        return productionModels;
    }

    public static List<ProductionTypeModel> getListProductionTypeModelFromResultSet(ResultSet resultSet) throws SQLException {
        List<ProductionTypeModel> productionTypeModels = new ArrayList<>();
        while (resultSet.next()){
            ProductionTypeModel productionTypeModel = new ProductionTypeModel();
            productionTypeModel.setId(resultSet.getInt("id"));
            productionTypeModel.setName(resultSet.getString("name"));
            productionTypeModels.add(productionTypeModel);
        }
        return productionTypeModels;
    }

    public static List<OfflineStoreModel> getListOfflineStoreModelFromResultSet(ResultSet resultSet) throws SQLException {
        List<OfflineStoreModel> offlineStoreModels = new ArrayList<>();
        while (resultSet.next()){
            OfflineStoreModel offlineStoreModel = new OfflineStoreModel();
            offlineStoreModel.setId(resultSet.getInt("id"));
            offlineStoreModel.setPhone(resultSet.getString("ofs_phone"));
            offlineStoreModel.setAddress(resultSet.getString("ofs_address"));
            offlineStoreModel.setName(resultSet.getString("ofs_name"));
            offlineStoreModel.setPhoto(resultSet.getString("ofs_photo"));
            offlineStoreModel.setEnable(resultSet.getInt("enable"));
            offlineStoreModels.add(offlineStoreModel);
        }
        return offlineStoreModels;
    }

    public static List<DesignerLevelModel> getListDesignerLevelModelFromResultSet(ResultSet resultSet) throws SQLException {
        List<DesignerLevelModel> designerLevelModels=new ArrayList<>();
            while (resultSet.next()){
                DesignerLevelModel designerLevelModel=new DesignerLevelModel();
                designerLevelModel.setId(resultSet.getInt("id"));
                designerLevelModel.setLevelName(resultSet.getString("level_name"));
                designerLevelModels.add(designerLevelModel);
            }
            return designerLevelModels;
    }
}
