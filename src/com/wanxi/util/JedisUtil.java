package com.wanxi.util;

import redis.clients.jedis.Jedis;

public class JedisUtil {

    public static long delByKey(String... key){
        Jedis jedis = new Jedis();
        try {
            return jedis.del(key);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            jedis.close();
        }
        return 0;
    }
}
