package com.wanxi.util;

import java.sql.*;

public class JDBCUtil {

    private static String username = "root";
    private static String pwd = "123456";
    private static String driver = "com.mysql.jdbc.Driver";
    private static String url = "jdbc:mysql://localhost:3306/costume?useSSL=true&useUnicode=true&characterEncoding=UTF8&zeroDateTimeBehavior=convertToNull &serverTimezone=Asia/Shanghai";
    private static Connection conn = null;
    private static Statement statement = null;
    private static ResultSet resultSet = null;
    public static Connection getConnection() {
        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(url, username, pwd);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }
    public static PreparedStatement getPreparedStatement(String sql) throws SQLException {
        return getConnection().prepareStatement(sql);
    }
    public static ResultSet executeQuery(String sql) throws SQLException {


        return getConnection().createStatement().executeQuery(sql);
    }
    public static int executeUpdate(String sql){

        try {
            int i = getConnection().createStatement().executeUpdate(sql);
            return i;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            close();
        }


        return 0;
    }

    public static void close() {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
