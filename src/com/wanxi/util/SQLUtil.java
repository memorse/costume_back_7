package com.wanxi.util;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SQLUtil {
    public static int PAGE_SIZE=1;
    public static int setEnableToFalse(String tableName,int id){
        String sql="UPDATE "+tableName+" set enable=0 where id <> "+id;
        int i = JDBCUtil.executeUpdate(sql);
        return i;
    }
    public static int getCount(String tabelName){
        String sql="select count(*) as count from "+tabelName;
        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            if (resultSet.next()){
                int count = resultSet.getInt("count");
                return count;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return 0;
    }
}
