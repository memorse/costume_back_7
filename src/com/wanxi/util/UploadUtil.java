package com.wanxi.util;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;
import java.util.UUID;

@WebServlet("/uploadImg")
public class UploadUtil extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        ServletUtil.setEncoding(req,resp);
                InputStream in = null;
                OutputStream out = null;

                try {
                        // 使用默认配置创建解析器工厂
                         DiskFileItemFactory factory = new DiskFileItemFactory();
                        // 获取解析器
                       ServletFileUpload upload = new ServletFileUpload(factory);
                         // 上传表单是否为multipart/form-data类型
                        if (!upload.isMultipartContent(req)) {
                                return;
                             }
                        // 解析request的输入流
                         List<FileItem> fileItemList = upload.parseRequest(req);
                         // 迭代list集合
                        for (FileItem fileItem : fileItemList) {
                                if (fileItem.isFormField()) {
                                         // 普通字段
                                         String name = fileItem.getFieldName();
                                        String value = fileItem.getString();
                                         System.out.println(name + "=" + value);
                                    } else {
                                       // 上传文件
                                        // 获取上传文件名
                                    String realPath=this.getClass().getResource("/").getPath();
                                    String savePath=realPath.substring(0,realPath.indexOf("WEB-INF"))+"files";
                                         String fileName = fileItem.getName();
                                       fileName =UUID.randomUUID().toString().replace("-","")+fileName.substring(fileName.lastIndexOf("\\")+1);
                                         // 获取输入流
                                         in = fileItem.getInputStream();

                                        // 获取上传文件目录
                                      // 上传文件名若不存在, 则先创建
                                      File savePathDir = new File(savePath);
                                        if (!savePathDir.exists()) {
                                               savePathDir.mkdir();
                                            }

                                     // 获取输出流
                                       out = new FileOutputStream(savePath + "\\" + fileName);
                                         int len = 0;
                                   byte[] buffer = new byte[1024];
                                       while((len=in.read(buffer)) > 0) {
                                                out.write(buffer, 0, len);
                                            }
                                    JSONObject jsonObject = new JSONObject();
                                       jsonObject.put("url","/files/"+fileName);
                                    resp.getWriter().println(jsonObject);
                                   }
                           }
                    } catch (Exception e) {
                         e.printStackTrace();
                    } finally {
                         if (in != null) {
                                in.close();
                          }
                         if (out != null) {
                                out.close();
                           }
                     }

    }
}
