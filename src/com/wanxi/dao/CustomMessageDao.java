package com.wanxi.dao;

import com.wanxi.model.CustomMessageModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomMessageDao {
    List<CustomMessageModel> getCustomMessages();

    List<CustomMessageModel> getCustomMessagesForIndex();

    List<CustomMessageModel> getCustomMsgByKey(String key);

    List<CustomMessageModel> getCustomMsgByKeyWithPage(@Param("key") String key, @Param("startPage") int startPage, @Param("pageSize") int pageSize, @Param("isPage") int isPage);

    int saveCustomMessage(CustomMessageModel customMessageModel);
    int updateCustomMessage(CustomMessageModel customMessageModel);
    int delCustomMessage(int id);


}
