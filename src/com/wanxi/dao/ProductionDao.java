package com.wanxi.dao;

import com.wanxi.model.ProductionModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductionDao {
    List<ProductionModel> getProductions();

    List<ProductionModel> getProductionsForIndex();

    List<ProductionModel> getProductionsBykey(String key);

    List<ProductionModel> getProductionsBykeyWithPage(@Param("key") String key, @Param("startPage") int startPage, @Param("pageSize") int pageSize, @Param("isPage") int isPage);

    List<ProductionModel> getProductionsByType(int id);

    int saveProduction(ProductionModel pro);
    int updateProduction(ProductionModel pro);
    int delProduction(int id);


}
