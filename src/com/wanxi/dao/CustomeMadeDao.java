package com.wanxi.dao;

import com.wanxi.model.CustomMadeModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomeMadeDao {
    List<CustomMadeModel> getCustomMades();

    List<CustomMadeModel> getCustomMadesForIndex();

    List<CustomMadeModel> getCustomMadesByKey(String key);

    List<CustomMadeModel> getCustomMadesByKeyWithPage(@Param("key") String key, @Param("startPage") int startPage, @Param("pageSize") int pageSize, @Param("isPage") int isPage);

    int saveCustomMade(CustomMadeModel customMadeModel);

    int updateCustomMade(CustomMadeModel customMadeModel);

    int delCustomMade(int id);

}
