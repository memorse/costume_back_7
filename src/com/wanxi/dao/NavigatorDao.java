package com.wanxi.dao;

import com.wanxi.model.NavigatorModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface NavigatorDao {
    List<NavigatorModel> getNavigators();

    List<NavigatorModel> getNavigatorsForIndex();

    List<NavigatorModel> getNavigatorsByKey(String key);

    List<NavigatorModel> getNavigatorsByKeyWithPage(@Param("key") String key, @Param("startPage") int startPage, @Param("pageSize") int pageSize, @Param("isPage") int isPage);

    int saveNavigator(NavigatorModel navigatorModel);
    int updateNavigator(NavigatorModel navigatorModel);
    int delNavigator(int id);
}
