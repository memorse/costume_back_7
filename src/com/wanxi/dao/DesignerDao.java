package com.wanxi.dao;

import com.wanxi.model.DesignerModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DesignerDao {
    List<DesignerModel> getDesigners();

    List<DesignerModel> getDesignersForIndex();

    List<DesignerModel> getDesignersByKey(String key);

    List<DesignerModel> getDesignersByKeyWithPage(@Param("key") String key, @Param("startPage") int startPage, @Param("pageSize") int pageSize, @Param("isPage") int isPage);

    int saveDesigner(DesignerModel designerModel);
    int updateDesigner(DesignerModel designerModel);
    int delDesigner(int id);
    DesignerModel getDesignersById(int id);
}
