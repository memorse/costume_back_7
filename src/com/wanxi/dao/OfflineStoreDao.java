package com.wanxi.dao;

import com.wanxi.model.OfflineStoreModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OfflineStoreDao {
    List<OfflineStoreModel> getOfflineStores();

    List<OfflineStoreModel> getOfflineStoresForIndex();

    List<OfflineStoreModel> getOfflineStoresByKey(String key);

    List<OfflineStoreModel> getOfflineStoresByKeyWithPage(@Param("key") String key, @Param("startPage") int startPage, @Param("pageSize") int pageSize, @Param("isPage") int isPage);

    int savaOfflineStoreModel(OfflineStoreModel offlineStore);
    int updateOfflineStoreModel(OfflineStoreModel offlineStore);
    int delOfflineStoreModel(int id);
}
