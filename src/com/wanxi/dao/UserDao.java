package com.wanxi.dao;

import com.wanxi.model.SearchDataModel;
import com.wanxi.model.UserModel;
import org.apache.ibatis.annotations.Param;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public interface UserDao {

    int delUser(String id);

    UserModel getUserByUesrAccount(String userAccount);
    List<UserModel> getUsers();
    List<UserModel> getUsersByKey(String key);

    List<UserModel> getUsersByKeyWithPage(String key, int startPage, int pageSize);

    List<UserModel> getUsersByKeyWithPagePlus(@Param("search") SearchDataModel searchDataModel, @Param("isPage") int isPage);
    int saveUser(UserModel user);
    int updateUser(UserModel user);

    UserModel verifyLogin(@Param("username") String username, @Param("password") String password);
}
