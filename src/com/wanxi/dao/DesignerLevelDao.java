package com.wanxi.dao;

import com.wanxi.model.DesignerLevelModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DesignerLevelDao {
    List<DesignerLevelModel> getDesignerLevels();
    List<DesignerLevelModel> getDesignerLevelsByKey(String key);
    List<DesignerLevelModel> getDesignerLevelsByKeyWithPage(@Param("key") String key, @Param("startPage") int startPage, @Param("pageSize") int pageSize, @Param("isPage") int isPage);
    DesignerLevelModel getDesignerLevelById(int id);
    int addDesignerLevel(DesignerLevelModel designerLevel);
    int updateDesignerLevel(DesignerLevelModel designerLevel);
    int delDesignerLevel(int id);
}
