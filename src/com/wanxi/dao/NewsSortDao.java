package com.wanxi.dao;

import com.wanxi.model.NewsSortModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface NewsSortDao {
    List<NewsSortModel> getNewsSorts();

    List<NewsSortModel> getNewsSortsForIndex();

    List<NewsSortModel> getNewsSortsByKey(String key);

    List<NewsSortModel> getNewsSortsByKeyWithPage(@Param("key") String key, @Param("startPage") int startPage, @Param("pageSize") int pageSize, @Param("isPage") int isPage);

    NewsSortModel getNewsSortsById(int id);
    int saveNewsSort(NewsSortModel newsSortModel);
    int updateNewsSort(NewsSortModel newsSortModel);
    int delNewsSort(int id);
}
