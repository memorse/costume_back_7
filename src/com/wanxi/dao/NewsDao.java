package com.wanxi.dao;

import com.wanxi.model.NewsModel;
import com.wanxi.model.NewsSortModel;
import org.apache.ibatis.annotations.Param;

import java.sql.SQLException;
import java.util.List;

public interface NewsDao {
    List<NewsModel> getNews();

    List<NewsModel> getNewsForIndex();

    List<NewsModel> getNewsByKey(String key);

    List<NewsModel> getNewsByKeyWithPage(@Param("key") String key, @Param("startPage") int startPage, @Param("pageSize") int pageSize, @Param("isPage") int isPage);

    int saveNews(NewsModel newsModel);
    int updateNews(NewsModel newsModel);
    int delNews(int id);
}
