package com.wanxi.dao;

import com.wanxi.model.ProductionTypeModel;

import java.util.List;

public interface ProductionTypeDao {
    List<ProductionTypeModel> getProductionTypes();
    ProductionTypeModel getProductionTypeById(int id);
}
