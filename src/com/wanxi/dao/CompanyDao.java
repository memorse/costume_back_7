package com.wanxi.dao;

import com.wanxi.model.CompanyModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CompanyDao {
    List<CompanyModel> getCompanys();
    CompanyModel getCompanyForIndex();
    List<CompanyModel> getCompanysByKey(String key);
    List<CompanyModel> getCompanysByKeyWithPage(@Param("key") String key, @Param("pageSize") int pageSize, @Param("currentPage") int currentPage);
    int saveCompany(CompanyModel companyModel);
    int delCompany(int id);
    int updateCompany(CompanyModel companyModel);
    int updateCompanyEnableToFalseExceptOne(int id);
}
