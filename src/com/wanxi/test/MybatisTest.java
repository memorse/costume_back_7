package com.wanxi.test;

import com.wanxi.dao.*;
import com.wanxi.model.*;
import com.wanxi.service.CompanyService;
import com.wanxi.util.MyBatisUtil;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MybatisTest {

    @Test
    public void test1() {
        SqlSession sqlSession = MyBatisUtil.getSqlSession();
        CompanyDao mapper = sqlSession.getMapper(CompanyDao.class);
//        List<CompanyModel> companys = mapper.getCompanysByKeyWithPage("",1,0);
//        for (CompanyModel company : companys) {
//            System.out.println(company.toString());
//        }
        sqlSession.close();
    }

    @Test
    public void test2() throws ParseException {
        SqlSession sqlSession = MyBatisUtil.getSqlSession();
        UserDao mapper = sqlSession.getMapper(UserDao.class);
//        SearchDataModel searchDataModel = new SearchDataModel();
//        searchDataModel.setBeginDate(new SimpleDateFormat("yyyy-MM-dd").parse("2020-9-1"));
//        searchDataModel.setKey("");
//        searchDataModel.setPageSize(3);
//        List<UserModel> usersByKeyWithPagePlus = mapper.getUsersByKeyWithPagePlus(searchDataModel, 1);
//        for (UserModel byKeyWithPagePlus : usersByKeyWithPagePlus) {
//            System.out.println(byKeyWithPagePlus.toString());
//        }
//        UserModel userModel = new UserModel();
//        userModel.setId(17);
//        userModel.setAccount("123123ahhh");
//        userModel.setName("123123ahhh");
//        userModel.setNickName("123123ahhh");
//        userModel.setHobby("asdf");
//        mapper.updateUser(userModel);

        mapper.delUser("17");
        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    public void test3(){
        SqlSession sqlSession = MyBatisUtil.getSqlSession();
        CustomeMadeDao mapper = sqlSession.getMapper(CustomeMadeDao.class);
        List<CustomMadeModel> customMadesByKeyWithPage = mapper.getCustomMadesByKeyWithPage("", 1, 3, 1);
        for (CustomMadeModel customMadeModel : customMadesByKeyWithPage) {
            System.out.println(customMadeModel.toString());
        }

        sqlSession.close();
    }

    @Test
    public void test4(){
        SqlSession sqlSession = MyBatisUtil.getSqlSession();
        DesignerLevelDao mapper= sqlSession.getMapper(DesignerLevelDao.class);
        List<DesignerLevelModel> designerLevelsByKeyWithPage = mapper.getDesignerLevelsByKeyWithPage("", 0, 10, 0);
        for (DesignerLevelModel designerLevelModel : designerLevelsByKeyWithPage) {
            System.out.println(designerLevelModel);
        }

        sqlSession.close();
    }
}