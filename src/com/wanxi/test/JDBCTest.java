package com.wanxi.test;

import com.wanxi.dao.*;
import com.wanxi.daoImpl.*;
import com.wanxi.model.*;
import com.wanxi.service.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class JDBCTest {
    public static void main(String[] args) {
//        CompanyService companyService = new CompanyService();
//        CompanyModel companyModel = companyService.getCompanys().get(0);
//        CompanyDao companyDao=new CompanyDaoImpl();
//        companyDao.saveCompany(companyModel);

//        DesignerService designerService = new DesignerService();
//        List<DesignerModel> designers = designerService.getDesigners();
//        DesignerDaoImpl designerDao = new DesignerDaoImpl();
//        for (DesignerModel designer : designers) {
//            designerDao.saveDesigner(designer);
//        }
//        CustomMessageService customMessageService = new CustomMessageService();
//        List<CustomMessageModel> cusMessage = customMessageService.getCusMessage();
//        CustomMessageDaoImpl customMessageDao = new CustomMessageDaoImpl();
//        for (CustomMessageModel customMessageModel : cusMessage) {
//            customMessageDao.saveCustomMessage(customMessageModel);
//        }
//        CustomMadeService customMadeService = new CustomMadeService();
//        List<CustomMadeModel> customMadeSteps = customMadeService.getCustomMadeSteps();
//        CustomMadeDaoImpl customMadeDao = new CustomMadeDaoImpl();
//        for (CustomMadeModel customMadeStep : customMadeSteps) {
//            customMadeDao.saveCustomMade(customMadeStep);
//        }

//        NewsService newsService = new NewsService();
//        List<NewsModel> news = newsService.getNews();
//        NewsDao newsDao = new NewsDaoImpl();
//        for (NewsModel newsModel : news) {
//            newsDao.saveNews(newsModel);
//        }

//        NewsSortService newsSortService = new NewsSortService();
//        List<NewsSortModel> newsTypes = newsSortService.getNewsTypes();
//        NewsSortDaoImpl newsSortDao = new NewsSortDaoImpl();
//        for (NewsSortModel newsType : newsTypes) {
//            newsSortDao.saveNewsSort(newsType);
//        }

//        OfflineStoreService offlineStoreService = new OfflineStoreService();
//        OfflineStoreDaoImpl offlineStoreDao = new OfflineStoreDaoImpl();
//        List<OfflineStoreModel> offlineStores = offlineStoreService.getOfflineStores();
//        for (OfflineStoreModel offlineStore : offlineStores) {
//
//            offlineStoreDao.savaOfflineStoreModel(offlineStore);
//        }

//        ProductionService productionService = new ProductionService();
//        List<ProductionModel> productions = productionService.getProductions();
//        for (ProductionModel production : productions) {
//            new ProductionDaoImpl().saveProduction(production);
//        }
//        NavigatorService navigatorService = new NavigatorService();
//        List<NavigatorModel> nav = navigatorService.getNav();
//        for (NavigatorModel navigatorModel : nav) {
//            new NavigatorDaoImpl().saveNavigator(navigatorModel);
//        }

//        UserModel userModel = new UserModel();
//        userModel.setAccount("1982764515");
//        userModel.setName("hedeping1");
//        userModel.setNickName("hedeping");
//        userModel.setMate("hedeping");
//        userModel.setGender("男");
//        userModel.setPassword("heping");
//        String[] hobbys={"篮球","足球","乒乓球"
//
//        };
//        userModel.setHobby(hobbys);
//        userModel.setBirthday(new Date(System.currentTimeMillis()));
//        userModel.setWeddingAnniversary(new Date(System.currentTimeMillis()));
//        int i = new UserService().saveUser(userModel);
        List<UserModel> users = new UserService().getUsers();
        for (UserModel user : users) {
            System.out.println(user.toString());
        }


    }
}
