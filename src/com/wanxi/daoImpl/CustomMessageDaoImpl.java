package com.wanxi.daoImpl;

import com.wanxi.dao.CustomMessageDao;
import com.wanxi.model.CustomMessageModel;
import com.wanxi.util.JDBCUtil;
import com.wanxi.util.ModelUtil;
import com.wanxi.util.OtherUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomMessageDaoImpl implements CustomMessageDao {
    private Connection connection = null;

    @Override
    public List<CustomMessageModel> getCustomMessages() {
        String sql = "select * from custom_message";

        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return ModelUtil.getListCustomMessageFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JDBCUtil.close();
        }
        return null;
    }

    /**
     * 首页展示信息
     * @return
     */
    @Override
    public List<CustomMessageModel> getCustomMessagesForIndex() {
        String sql = "select * from custom_message where enable =1";

        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return ModelUtil.getListCustomMessageFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JDBCUtil.close();
        }
        return null;
    }

    @Override
    public List<CustomMessageModel> getCustomMsgByKey(String key) {
        key="%"+key+"%";
        String sql="select * from custom_message where " +
                "id like ? or cmsg_firstname like ? or cmsg_hus_msg like ? or cmsg_wife_msg like ?";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,key);
            preparedStatement.setString(2,key);
            preparedStatement.setString(3,key);
            preparedStatement.setString(4,key);
            ResultSet resultSet = preparedStatement.executeQuery();
            return ModelUtil.getListCustomMessageFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }
    @Override
    public List<CustomMessageModel> getCustomMsgByKeyWithPage(String key,int startRows,int pageSize,int isPage) {
        key="%"+key+"%";
        String sql="select * from custom_message where " +
                "id like ? or " +
                "cmsg_firstname like ? or " +
                "cmsg_hus_msg like ? or " +
                "cmsg_wife_msg like ? " +
                "limit ?,?";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,key);
            preparedStatement.setString(2,key);
            preparedStatement.setString(3,key);
            preparedStatement.setString(4,key);
            preparedStatement.setInt(5,startRows);
            preparedStatement.setInt(6,pageSize);
            ResultSet resultSet = preparedStatement.executeQuery();
            return ModelUtil.getListCustomMessageFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }

    @Override
    public int saveCustomMessage(CustomMessageModel customMessageModel) {

        String sql = "insert into custom_message(cmsg_firstname,cmsg_wife_msg," +
                "cmsg_hus_msg,cmsg_photo,enable)" +
                "values(?,?,?,?,?)";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1, customMessageModel.getFirstName());
            preparedStatement.setString(2, customMessageModel.getWifeMessage());
            preparedStatement.setString(3, customMessageModel.getHusMessage());
            preparedStatement.setString(4, customMessageModel.getPhoto());
            preparedStatement.setInt(5, customMessageModel.getEnable());
            int i = preparedStatement.executeUpdate();
            OtherUtil.jeadge(i, 0, "客户留言");
            return i;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JDBCUtil.close();
        }
        return 0;
    }

    @Override
    public int updateCustomMessage(CustomMessageModel customMessageModel) {
        String sql="update custom_message set " +
                "cmsg_firstname=?," +
                "cmsg_hus_msg=?," +
                "cmsg_wife_msg=?," +
                "cmsg_photo=?," +
                "enable=? " +
                "where id=?";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,customMessageModel.getFirstName());
            preparedStatement.setString(2,customMessageModel.getHusMessage());
            preparedStatement.setString(3,customMessageModel.getWifeMessage());
            preparedStatement.setString(4,customMessageModel.getPhoto());
            preparedStatement.setInt(5,customMessageModel.getEnable());
            preparedStatement.setInt(6,customMessageModel.getId());
            int i = preparedStatement.executeUpdate();
            return i;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return 0;
    }

    @Override
    public int delCustomMessage(int id) {
        String sql="delete from custom_message where id="+id;
        return JDBCUtil.executeUpdate(sql);
    }
}
