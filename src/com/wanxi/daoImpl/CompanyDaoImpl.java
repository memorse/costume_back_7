package com.wanxi.daoImpl;

import com.wanxi.dao.CompanyDao;
import com.wanxi.model.CompanyModel;
import com.wanxi.util.JDBCUtil;
import com.wanxi.util.ModelUtil;
import com.wanxi.util.OtherUtil;
import com.wanxi.util.SQLUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CompanyDaoImpl implements CompanyDao {

    private Connection connection=null;
    @Override
    public List<CompanyModel> getCompanys() {
        String sql="select * from company";
        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return ModelUtil.getListCompanyFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }

    @Override
    public CompanyModel getCompanyForIndex() {
        String sql="select * from company where enable =1";
        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return ModelUtil.getListCompanyFromResultSet(resultSet).get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }
    @Override
    public List<CompanyModel> getCompanysByKey(String key) {
        String sql="select * from company where id like '%"+key+"%' or " +
                "c_name like '%"+key+"%' or c_address like '%"+key+"%' " +
                "or c_ad like '%"+key+"%' or c_phone like '%"+key+"%' " +
                "or c_email like '%"+key+"%' or c_qq like '%"+key+"%'";
        List<CompanyModel> companyModels=new ArrayList<>();
        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            companyModels = ModelUtil.getListCompanyFromResultSet(resultSet);

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }

        return companyModels;
    }

    @Override
    public List<CompanyModel> getCompanysByKeyWithPage(String key,int pageSize,int currentPage) {
        String sql="select * from company where id like '%"+key+"%' or " +
                "c_name like '%"+key+"%' or c_address like '%"+key+"%' " +
                "or c_ad like '%"+key+"%' or c_phone like '%"+key+"%' " +
                "or c_email like '%"+key+"%' or c_qq like '%"+key+"%' " +
                "limit "+currentPage+","+pageSize+"";
        List<CompanyModel> companyModels=new ArrayList<>();
        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            companyModels = ModelUtil.getListCompanyFromResultSet(resultSet);

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }

        return companyModels;
    }

    @Override
    public int saveCompany(CompanyModel companyModel) {
        String sql="insert into company(c_name,c_phone,c_qq,c_address," +
                "c_ad,c_email,c_introduce,enable)" +
                " values(?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement preparedStatement =JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,companyModel.getName());
            preparedStatement.setString(2,companyModel.getPhone());
            preparedStatement.setString(3,companyModel.getQq());
            preparedStatement.setString(4,companyModel.getAddress());
            preparedStatement.setString(5,companyModel.getAd());
            preparedStatement.setString(6,companyModel.getEmail());
            preparedStatement.setString(7,companyModel.getIntroduce());
            preparedStatement.setInt(8,companyModel.getEnable());
            int result=preparedStatement.executeUpdate();
            OtherUtil.jeadge(result,0,"公司");
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
           JDBCUtil.close();
        }
    return 0;
    }

    @Override
    public int delCompany(int id) {
        String sql="delete from company where id="+id;
        int i = JDBCUtil.executeUpdate(sql);
        return i;
    }

    @Override
    public int updateCompany(CompanyModel companyModel) {
        String  sql="update company set c_name=?,c_phone=?,c_email=?,c_qq=?,c_address=?,c_ad=?," +
                "c_introduce=?,enable=? where id=?";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,companyModel.getName());
            preparedStatement.setString(2,companyModel.getPhone());
            preparedStatement.setString(3,companyModel.getEmail());
            preparedStatement.setString(4,companyModel.getQq());
            preparedStatement.setString(5,companyModel.getAddress());
            preparedStatement.setString(6,companyModel.getAd());
            preparedStatement.setString(7,companyModel.getIntroduce());
            preparedStatement.setInt(8,companyModel.getEnable());
            preparedStatement.setInt(9,companyModel.getId());
            int i = preparedStatement.executeUpdate();

            if(companyModel.getEnable()==1){
                int rows= SQLUtil.setEnableToFalse("company", companyModel.getId());
                return i>0&&rows>0?1:0;
            }else {
                return i;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return 0;
    }

    @Override
    public int updateCompanyEnableToFalseExceptOne(int id) {
        return 0;
    }
}
