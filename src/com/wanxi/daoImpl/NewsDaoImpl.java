package com.wanxi.daoImpl;

import com.wanxi.dao.NewsDao;
import com.wanxi.model.NewsModel;
import com.wanxi.util.JDBCUtil;
import com.wanxi.util.ModelUtil;
import com.wanxi.util.OtherUtil;
import jdk.nashorn.internal.scripts.JD;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class NewsDaoImpl implements NewsDao {
    @Override
    public List<NewsModel> getNews() {
        String sql="select * from news";
        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return ModelUtil.getListNewsModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }

    @Override
    public List<NewsModel> getNewsForIndex() {
        String sql="select * from news where enable=1";
        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return ModelUtil.getListNewsModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }

    @Override
    public List<NewsModel> getNewsByKey(String key) {
        key="%"+key+"%";
        String sql="select * from news where " +
                "id like ? or" +
                " news_title like ? or" +
                " news_content like ? or" +
                " news_img_route like ? or" +
                " news_pubtime like ?";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,key);
            preparedStatement.setString(2,key);
            preparedStatement.setString(3,key);
            preparedStatement.setString(4,key);
            preparedStatement.setString(5,key);
            ResultSet resultSet = preparedStatement.executeQuery();
            return  ModelUtil.getListNewsModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();

        }
        return null;
    }

    @Override
    public List<NewsModel> getNewsByKeyWithPage(String key, int startPage, int pageSize, int isPage) {
        return null;
    }

    public List<NewsModel> getNewsByKeyWithPage(String key,int startRows,int pageSize) {
        key="%"+key+"%";
        String sql="select * from news where " +
                "id like ? or" +
                " news_title like ? or" +
                " news_content like ? or" +
                " news_img_route like ? or" +
                " news_pubtime like ? " +
                "limit ?,?";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,key);
            preparedStatement.setString(2,key);
            preparedStatement.setString(3,key);
            preparedStatement.setString(4,key);
            preparedStatement.setString(5,key);
            preparedStatement.setInt(6,startRows);
            preparedStatement.setInt(7,pageSize);
            ResultSet resultSet = preparedStatement.executeQuery();
            return  ModelUtil.getListNewsModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();

        }
        return null;
    }

    @Override
    public int saveNews(NewsModel newsModel) {
        String  sql="insert into news(news_title,news_content,news_img_route,news_pubtime,enable,news_sort)" +
                "values(?,?,?,?,?,?)";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,newsModel.getTitle());
            preparedStatement.setString(2,newsModel.getContent());
            preparedStatement.setString(3,newsModel.getImgRoute());
            preparedStatement.setDate(4, new Date(newsModel.getPubTime().getTime()));
            preparedStatement.setInt(5, newsModel.getEnable());
            preparedStatement.setInt(6, newsModel.getNewsSort().getId());
            int i = preparedStatement.executeUpdate();
            OtherUtil.jeadge(i,0,"新闻");
            return i;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return 0;
    }

    @Override
    public int updateNews(NewsModel newsModel) {
        String sql="update news set " +
                "news_title=?,news_content=?," +
                "news_img_route=?,news_pubtime=?," +
                "enable=?," +
                "news_sort=?" +
                " where id=?";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,newsModel.getTitle());
            preparedStatement.setString(2,newsModel.getContent());
            preparedStatement.setString(3,newsModel.getImgRoute());
            preparedStatement.setDate(4,new Date(newsModel.getPubTime().getTime()));
            preparedStatement.setInt(5,newsModel.getEnable());
            preparedStatement.setInt(6,newsModel.getNewsSort().getId());
            preparedStatement.setInt(7,newsModel.getId());
            int i = preparedStatement.executeUpdate();
            return i;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return 0;
    }

    @Override
    public int delNews(int id)  {
        String sql="delete from news where id="+id;

        return JDBCUtil.executeUpdate(sql);
    }
}
