package com.wanxi.daoImpl;

import com.wanxi.dao.OfflineStoreDao;
import com.wanxi.model.OfflineStoreModel;
import com.wanxi.util.JDBCUtil;
import com.wanxi.util.ModelUtil;
import com.wanxi.util.OtherUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OfflineStoreDaoImpl implements OfflineStoreDao {
    @Override
    public List<OfflineStoreModel> getOfflineStores() {
        String sql="select * from offline_store";
        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return ModelUtil.getListOfflineStoreModelFromResultSet(resultSet);

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
           JDBCUtil.close();
        }
        return null;
    }

    @Override
    public List<OfflineStoreModel> getOfflineStoresForIndex() {
        String sql="select * from offline_store where enable=1";
        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return ModelUtil.getListOfflineStoreModelFromResultSet(resultSet);

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
           JDBCUtil.close();
        }
        return null;
    }

    @Override
    public List<OfflineStoreModel> getOfflineStoresByKey(String key) {
        key="%"+key+"%";
        String sql="select * from offline_store where " +
                "id like ? or " +
                "ofs_name like ? or " +
                "ofs_photo like ? or " +
                "ofs_address like ? or " +
                "ofs_phone like ?";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,key);
            preparedStatement.setString(2,key);
            preparedStatement.setString(3,key);
            preparedStatement.setString(4,key);
            preparedStatement.setString(5,key);
            ResultSet resultSet = preparedStatement.executeQuery();
            return ModelUtil.getListOfflineStoreModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }

    @Override
    public List<OfflineStoreModel> getOfflineStoresByKeyWithPage(String key, int startPage, int pageSize, int isPage) {
        return null;
    }

    public List<OfflineStoreModel> getOfflineStoresByKeyWithPage(String key,int startPage,int pageSize) {
        key="%"+key+"%";
        String sql="select * from offline_store where " +
                "id like ? or " +
                "ofs_name like ? or " +
                "ofs_photo like ? or " +
                "ofs_address like ? or " +
                "ofs_phone like ? " +
                "limit ?,?";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,key);
            preparedStatement.setString(2,key);
            preparedStatement.setString(3,key);
            preparedStatement.setString(4,key);
            preparedStatement.setString(5,key);
            preparedStatement.setInt(6,startPage);
            preparedStatement.setInt(7,pageSize);
            ResultSet resultSet = preparedStatement.executeQuery();
            return ModelUtil.getListOfflineStoreModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }

    @Override
    public int savaOfflineStoreModel(OfflineStoreModel offlineStore) {

        String sql="insert into offline_store(ofs_name,ofs_photo,ofs_address,ofs_phone,enable)" +
                "values(?,?,?,?,?)";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,offlineStore.getName());
            preparedStatement.setString(2,offlineStore.getPhoto());
            preparedStatement.setString(3,offlineStore.getAddress());
            preparedStatement.setString(4,offlineStore.getPhone());
            preparedStatement.setInt(5,offlineStore.getEnable());
            int i = preparedStatement.executeUpdate();
            OtherUtil.jeadge(i,0,"线下门店");
            return i;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return 0;
    }

    @Override
    public int updateOfflineStoreModel(OfflineStoreModel offlineStore) {
        String sql="update offline_store set " +
                "ofs_name=?," +
                "ofs_photo=?," +
                "ofs_address=?," +
                "ofs_phone=?," +
                "enable=? " +
                "where id=?";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,offlineStore.getName());

            preparedStatement.setString(2,offlineStore.getPhoto());
            preparedStatement.setString(3,offlineStore.getAddress());
            preparedStatement.setString(4,offlineStore.getPhone());
            preparedStatement.setInt(5,offlineStore.getEnable());
            preparedStatement.setInt(6,offlineStore.getId());
            int i = preparedStatement.executeUpdate();
            return  i;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return 0;
    }

    @Override
    public int delOfflineStoreModel(int id) {
        String sql="delete from offline_store where id="+id;
        return JDBCUtil.executeUpdate(sql);
    }
}
