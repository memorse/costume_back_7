package com.wanxi.daoImpl;

import com.wanxi.dao.DesignerDao;
import com.wanxi.model.DesignerModel;
import com.wanxi.util.JDBCUtil;
import com.wanxi.util.ModelUtil;
import com.wanxi.util.OtherUtil;
import jdk.nashorn.internal.scripts.JD;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DesignerDaoImpl implements DesignerDao {
    @Override
    public List<DesignerModel> getDesigners() {
        String sql="select * from designer";
        try {

            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return ModelUtil.getListDesignerModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }

    /**
     * 网站展示信息
     * @return
     */
    @Override
    public List<DesignerModel> getDesignersForIndex() {
        String sql="select * from designer where enable=1";
        try {

            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return ModelUtil.getListDesignerModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }

    @Override
    public List<DesignerModel> getDesignersByKey(String key) {
        key="%"+key+"%";
        String sql="select * from designer where " +
                "designer_name like ? or designer_gender like ? " +
                "or designer_photo like ? or designer_level like ? " +
                "or designer_introduce  like ? or id like ?";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,key);
            preparedStatement.setString(2,key);
            preparedStatement.setString(3,key);
            preparedStatement.setString(4,key);
            preparedStatement.setString(5,key);
            preparedStatement.setString(6,key);
            ResultSet resultSet = preparedStatement.executeQuery();
            return ModelUtil.getListDesignerModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }

    @Override
    public List<DesignerModel> getDesignersByKeyWithPage(String key, int startPage, int pageSize, int isPage) {
        return null;
    }


    public List<DesignerModel> getDesignersByKeyWithPage(String key,int startRows,int pageSize) {
        key="%"+key+"%";
        String sql="select * from designer where " +
                "designer_name like ? or designer_gender like ? " +
                "or designer_photo like ? or designer_level like ? " +
                "or designer_introduce  like ? or id like ? " +
                "limit ?,?";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,key);
            preparedStatement.setString(2,key);
            preparedStatement.setString(3,key);
            preparedStatement.setString(4,key);
            preparedStatement.setString(5,key);
            preparedStatement.setString(6,key);
            preparedStatement.setInt(7,startRows);
            preparedStatement.setInt(8,pageSize);
            ResultSet resultSet = preparedStatement.executeQuery();
            return ModelUtil.getListDesignerModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }

    @Override
    public int saveDesigner(DesignerModel designerModel) {
        String sql="insert into designer(designer_name,designer_gender," +
                "designer_photo,designer_level,designer_introduce,enable)" +
                "values(?,?,?,?,?,?)";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,designerModel.getName());
            preparedStatement.setString(2,designerModel.getGender().equals("男")?"1":"0");
            preparedStatement.setString(3,designerModel.getPhoto());
            preparedStatement.setInt(4,designerModel.getLevel().getId());
            preparedStatement.setString(5,designerModel.getIntroduce());
            preparedStatement.setInt(5,designerModel.getEnable());
            int i = preparedStatement.executeUpdate();
            OtherUtil.jeadge(i,0,"设计师");
            return i;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return 0;
    }

    @Override
    public int updateDesigner(DesignerModel designerModel) {
        String sql="update designer set " +
                "designer_name=?,designer_gender=?,designer_photo=?," +
                "designer_level=?,designer_introduce=?,enable=? " +
                "where id=?";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,designerModel.getName());
            preparedStatement.setInt(2,designerModel.getGender()=="男"?1:0);
            preparedStatement.setString(3,designerModel.getPhoto());
            preparedStatement.setInt(4,designerModel.getLevel().getId());
            preparedStatement.setString(5,designerModel.getIntroduce());
            preparedStatement.setInt(6,designerModel.getEnable());
            preparedStatement.setInt(7,designerModel.getId());
            int i = preparedStatement.executeUpdate();
            return i;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return 0;
    }

    @Override
    public int delDesigner(int id) {
        String  sql="delete from designer where id="+id;
        return JDBCUtil.executeUpdate(sql);
    }

    @Override
    public DesignerModel getDesignersById(int id) {
        String sql="select * from designer where id="+id;
        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return ModelUtil.getListDesignerModelFromResultSet(resultSet).get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }
}
