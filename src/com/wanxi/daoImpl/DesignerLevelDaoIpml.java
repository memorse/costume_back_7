package com.wanxi.daoImpl;

import com.wanxi.dao.DesignerLevelDao;
import com.wanxi.model.DesignerLevelModel;
import com.wanxi.util.JDBCUtil;
import com.wanxi.util.ModelUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class DesignerLevelDaoIpml implements DesignerLevelDao {
    @Override
    public List<DesignerLevelModel> getDesignerLevels() {
        String sql="select * from designer_level";
        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return ModelUtil.getListDesignerLevelModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }

    @Override
    public List<DesignerLevelModel> getDesignerLevelsByKey(String key) {
        String sql="select * from designer_level where id like '%"+key+"%' or level_name like '%"+key+"%'";
        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return ModelUtil.getListDesignerLevelModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }

    @Override
    public List<DesignerLevelModel> getDesignerLevelsByKeyWithPage(String key, int startPage, int pageSize, int isPage) {
        return null;
    }

    @Override
    public DesignerLevelModel getDesignerLevelById(int id) {
        String sql="select * from designer_level where id="+id;
        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return ModelUtil.getListDesignerLevelModelFromResultSet(resultSet).get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }

        return null;
    }

    @Override
    public int addDesignerLevel(DesignerLevelModel designerLevel) {
        String sql="insert into designer_level(level_name) values("+designerLevel.getLevelName()+")";

        return  JDBCUtil.executeUpdate(sql);
    }

    @Override
    public int updateDesignerLevel(DesignerLevelModel designerLevel) {
        String sql="update designer_level set level_name="+designerLevel.getLevelName();
        return JDBCUtil.executeUpdate(sql);
    }

    @Override
    public int delDesignerLevel(int id) {
        String sql="delete from designer_level where id="+id;
        return JDBCUtil.executeUpdate(sql);
    }
}
