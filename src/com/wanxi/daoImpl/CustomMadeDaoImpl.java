package com.wanxi.daoImpl;

import com.wanxi.dao.CustomeMadeDao;
import com.wanxi.model.CustomMadeModel;
import com.wanxi.util.JDBCUtil;
import com.wanxi.util.ModelUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomMadeDaoImpl implements CustomeMadeDao {
    private Connection connection=null;
    @Override
    public List<CustomMadeModel> getCustomMades() {


        String sql="select * from custom_made";

        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return ModelUtil.getListCustomMadeFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }

    /**
     * 查询首页展示的数据
     * @return
     */
    @Override
    public List<CustomMadeModel> getCustomMadesForIndex() {


        String sql="select * from custom_made where enable=1 order by cmade_step_order ASC";

        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return ModelUtil.getListCustomMadeFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }

    @Override
    public List<CustomMadeModel> getCustomMadesByKey(String key) {
        key="%"+key+"%";
        String sql="select * from custom_made where " +
                "id like ? or cmade_step_name like ? " +
                "or cmade_step_order like ? or cmade_step_detail like ?";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,key);
            preparedStatement.setString(2,key);
            preparedStatement.setString(3,key);
            preparedStatement.setString(4,key);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<CustomMadeModel> listCustomMadeFromResultSet = ModelUtil.getListCustomMadeFromResultSet(resultSet);
            return listCustomMadeFromResultSet;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }
    @Override
    public List<CustomMadeModel> getCustomMadesByKeyWithPage(String key,int startRows,int pageSize,int isPage) {
        key="%"+key+"%";
        String sql="select * from custom_made where " +
                "id like ? or cmade_step_name like ? " +
                "or cmade_step_order like ? or cmade_step_detail like ? " +
                "limit ?,?";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,key);
            preparedStatement.setString(2,key);
            preparedStatement.setString(3,key);
            preparedStatement.setString(4,key);
            preparedStatement.setInt(5,startRows);
            preparedStatement.setInt(6,pageSize);
            ResultSet resultSet = preparedStatement.executeQuery();
            return ModelUtil.getListCustomMadeFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }

    @Override
    public int saveCustomMade(CustomMadeModel customMadeModel) {
        String sql="insert into custom_made (cmade_step_logo," +
                "cmade_step_name,cmade_step_order,cmade_step_detail,enable)" +
                "values(?,?,?,?,?)";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,customMadeModel.getStepLogo());
            preparedStatement.setString(2,customMadeModel.getStepName());
            preparedStatement.setInt(3,customMadeModel.getStepOrder());
            preparedStatement.setString(4,customMadeModel.getStepDetail());
            preparedStatement.setInt(5,customMadeModel.getEnable());
            int res = preparedStatement.executeUpdate();
            if(res>0){
                System.out.println("存入定制步骤信息成功");
            }else
            {
                System.out.println("存入定制步骤信息失败");
            }
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
    return 0;
    }

    @Override
    public int updateCustomMade(CustomMadeModel customMadeModel) {
        String sql="update custom_made set cmade_step_name=?," +
                "cmade_step_logo=?,cmade_step_detail=?,cmade_step_order=?," +
                "enable=? where id=?";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,customMadeModel.getStepName());
            preparedStatement.setString(2,customMadeModel.getStepLogo());
            preparedStatement.setString(3,customMadeModel.getStepDetail());
            preparedStatement.setInt(4,customMadeModel.getStepOrder());
            preparedStatement.setInt(5,customMadeModel.getEnable());
            preparedStatement.setInt(6,customMadeModel.getId());
            int i = preparedStatement.executeUpdate();
            return i;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return 0;
    }

    @Override
    public int delCustomMade(int id) {
        String sql="delete from custom_made where id="+id;

        return JDBCUtil.executeUpdate(sql);
    }
}
