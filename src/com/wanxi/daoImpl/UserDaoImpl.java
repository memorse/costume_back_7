package com.wanxi.daoImpl;

import com.wanxi.dao.UserDao;
import com.wanxi.model.SearchDataModel;
import com.wanxi.model.UserModel;
import com.wanxi.util.JDBCUtil;
import com.wanxi.util.ModelUtil;
import com.wanxi.util.OtherUtil;
import jdk.nashorn.internal.scripts.JD;
import org.junit.Test;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDao {
    private Connection connection = null;

    @Override
    public int delUser(String id) {
        String sql = "delete from user where id =" + id;
//        connection=JDBCUtil.getConnection();
        try {
//            int i = connection.createStatement().executeUpdate(sql);
            int i = JDBCUtil.executeUpdate(sql);
            return i;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
   public UserModel getUserByUesrAccount(String userAccount) {
        connection = JDBCUtil.getConnection();
        String sql = "select * from user where user_account='" + userAccount + "'";
        try {
            ResultSet resultSet = connection.createStatement().executeQuery(sql);
            while (resultSet.next()) {
                UserModel userModel = new UserModel();
                userModel.setId(resultSet.getInt("id"));
                userModel.setAccount(resultSet.getString("user_account"));
                userModel.setHobby(resultSet.getString("user_hobby"));
                userModel.setGender(resultSet.getInt("user_gender") == 1 ? "男" : "女");
                userModel.setMate(resultSet.getString("user_mate"));
                userModel.setName(resultSet.getString("user_name"));
                userModel.setNickName(resultSet.getString("user_nickname"));
                userModel.setBirthday(resultSet.getDate("user_birthday"));
                userModel.setPhone(resultSet.getString("user_phone"));
                userModel.setPassword(resultSet.getString("user_pwd"));
                userModel.setWeddingAnniversary(resultSet.getDate("user_wedding_anniversary"));
                return userModel;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public List<UserModel> getUsers() {
        String sql = "select * from user";
        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return ModelUtil.getListUserModelFromResultSet(resultSet);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JDBCUtil.close();
        }
        return null;
    }

    @Override
    public List<UserModel> getUsersByKey(String key) {
        String sql = "select * from user " +
                "where user_account like '%" + key + "%' " +
                "or user_name like '%" + key + "%' " +
                "or user_nickname like '%" + key + "%' " +
                "or user_mate like '%" + key + "%' " +
                "or user_phone like '%" + key + "%' " +
                "or id like '%" + key + "%'";

        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return ModelUtil.getListUserModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JDBCUtil.close();
        }
        return null;
    }

    @Override
    public List<UserModel> getUsersByKeyWithPage(String key, int startPage, int pageSize) {
        key = "%" + key + "%";
        String sql = "select * from user where " +
                "user_account like ? " +
                "or user_name like ? " +
                "or user_nickname like ? " +
                "or user_mate like ? " +
                "or user_phone like ? " +
                "or id like ? " +
                "limit ?,?";

        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1, key);
            preparedStatement.setString(2, key);
            preparedStatement.setString(3, key);
            preparedStatement.setString(4, key);
            preparedStatement.setString(5, key);
            preparedStatement.setString(6, key);
            preparedStatement.setInt(7, startPage);
            preparedStatement.setInt(8, pageSize);
            ResultSet resultSet = preparedStatement.executeQuery();
            return ModelUtil.getListUserModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JDBCUtil.close();
        }
        return null;
    }

    /**
     *
     * @param searchDataModel  搜索数组模型
     * @param isPage 是否分页：  1：是， 0：否
     * @return
     */
    @Override
    public List<UserModel> getUsersByKeyWithPagePlus(SearchDataModel searchDataModel, int isPage) {
        String key = "%" + searchDataModel.getKey() + "%";
        String sql = "select * from user where " +
                "(user_account like ? " +
                "or user_name like ? " +
                "or user_nickname like ? " +
                "or user_mate like ? " +
                "or user_phone like ? " +
                "or id like ?) ";
        if (!searchDataModel.getGender().equals("all")) {
            String gender=searchDataModel.getGender().equals("男")?"1":"0";
            sql += " and user_gender="+gender;
        }
//        int hobbyLeng=searchDataModel.getHobbys().length;
//        if(hobbyLeng>0){
//            sql+=" and (";
//            for (int i=0;i<hobbyLeng;i++) {
//               if(i!=hobbyLeng-1)
//                   sql+="user_hobby like '%"+searchDataModel.getHobbys()[i]+"%' or";
//               else
//                   sql+="user_hobby like '%"+searchDataModel.getHobbys()[i]+"%'";
//            }
//            sql+=")";
//        }
        if(searchDataModel.getBeginDate()!=null&&searchDataModel.getEndDate()!=null){
            sql+=" and (user_birthday BETWEEN '"+new Date(searchDataModel.getBeginDate().getTime())+"' " +
                    "and '"+new Date(searchDataModel.getEndDate().getTime())+"' or " +
                    "user_wedding_anniversary BETWEEN '"+new Date(searchDataModel.getBeginDate().getTime())+"' " +
                    "and '"+new Date(searchDataModel.getEndDate().getTime())+"')";
        }
        else if(searchDataModel.getBeginDate()!=null&&searchDataModel.getEndDate()==null){
            sql+=" and (user_birthday >='"+new Date(searchDataModel.getBeginDate().getTime())+"' or " +
                    "user_wedding_anniversary >= '"+new Date(searchDataModel.getBeginDate().getTime())+"')";;
        }
        else if (searchDataModel.getBeginDate()==null&&searchDataModel.getEndDate()!=null){
            sql+=" and (user_birthday <='"+new Date(searchDataModel.getEndDate().getTime())+"' or " +
                    "user_wedding_anniversary <= '"+new Date(searchDataModel.getEndDate().getTime())+"')";;
        }
        if(isPage==1){
            sql+=" limit ?,?";
        }



        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1, key);
            preparedStatement.setString(2, key);
            preparedStatement.setString(3, key);
            preparedStatement.setString(4, key);
            preparedStatement.setString(5, key);
            preparedStatement.setString(6, key);
            if(isPage==1){
                preparedStatement.setInt(7, searchDataModel.getStartPage());
                preparedStatement.setInt(8, searchDataModel.getPageSize());
            }

            ResultSet resultSet = preparedStatement.executeQuery();
            return ModelUtil.getListUserModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JDBCUtil.close();
        }
        return null;
    }
//
//    public List<UserModel> getUserListfromResutlset(ResultSet resultSet) throws SQLException {
//        List<UserModel> userModels = new ArrayList<>();
//        while (resultSet.next()) {
//            UserModel userModel = new UserModel();
//            userModel.setId(resultSet.getInt("id"));
//            userModel.setAccount(resultSet.getString("user_account"));
//            userModel.setHobby(resultSet.getString("user_hobby"));
//            userModel.setGender(resultSet.getInt("user_gender") == 1 ? "男" : "女");
//            userModel.setMate(resultSet.getString("user_mate"));
//            userModel.setName(resultSet.getString("user_name"));
//            userModel.setNickName(resultSet.getString("user_nickname"));
//            userModel.setBirthday(resultSet.getDate("user_birthday"));
//            userModel.setPassword(resultSet.getString("user_pwd"));
//            userModel.setPhone(resultSet.getString("user_phone"));
//            userModel.setWeddingAnniversary(resultSet.getDate("user_wedding_anniversary"));
//            userModels.add(userModel);
//        }
//        return userModels;
//    }
//

    @Override
    public int saveUser(UserModel user) {
        String sql = "insert into user (user_account,user_name,user_nickname,user_gender," +
                "user_hobby,user_mate,user_birthday," +
                "user_wedding_anniversary,user_pwd," +
                "user_phone,user_is_admin,enable)" +
                "values(?,?,?,?,?,?,?,?,?,?,?,?)";

        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1, user.getAccount());
            preparedStatement.setString(2, user.getName());
            preparedStatement.setString(3, user.getNickName());
            preparedStatement.setString(4, user.getGender().equals("男") ? "1" : "0");
            preparedStatement.setString(5, user.getHobby());
            preparedStatement.setString(6, user.getMate());
            if (user.getBirthday() != null) {
                preparedStatement.setDate(7, new Date(user.getBirthday().getTime()));
            } else {
                preparedStatement.setDate(7, null);
            }
            if (user.getWeddingAnniversary() != null) {
                preparedStatement.setDate(8, new Date(user.getWeddingAnniversary().getTime()));
            } else {
                preparedStatement.setDate(8, null);
            }
            preparedStatement.setString(9, user.getPassword());
            preparedStatement.setString(10, user.getPhone());
            preparedStatement.setInt(11, user.getIsAdmin());
            preparedStatement.setInt(12, user.getEnable());
            int i = preparedStatement.executeUpdate();
            OtherUtil.jeadge(i, 0, "用户信息");
            return i;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JDBCUtil.close();
        }
        return 0;
    }

    @Override
    public int updateUser(UserModel user) {
        String sql = "update user set" +
                " user_account=?,user_name=?,user_nickname=?,user_pwd=?,user_phone=?,user_gender=?" +
                ",user_hobby=?,user_birthday=?,user_wedding_anniversary=?,user_mate=?," +
                "user_is_admin=?,enable=? " +
                " where id=?";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1, user.getAccount());
            preparedStatement.setString(2, user.getName());
            preparedStatement.setString(3, user.getNickName());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getPhone());
            preparedStatement.setInt(6, user.getGender() == "男" ? 1 : 0);
            preparedStatement.setString(7, user.getHobby());
            preparedStatement.setDate(8, new Date(user.getBirthday().getTime()));
            preparedStatement.setDate(9, new Date(user.getWeddingAnniversary().getTime()));
            preparedStatement.setString(10, user.getMate());
            preparedStatement.setInt(11, user.getIsAdmin());
            preparedStatement.setInt(12, user.getEnable());
            preparedStatement.setInt(13, user.getId());
            System.out.println(preparedStatement.toString());
            int i = preparedStatement.executeUpdate();
            return i;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JDBCUtil.close();
        }

        return 0;
    }

    @Override
    public UserModel verifyLogin(String username, String password) {
        String sql = "select * from user where user_account='" + username + "' and user_pwd='" + password + "'";
        UserModel userModel = null;
        boolean flag = false;
        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            if (resultSet.next()) {
                userModel = new UserModel();
                userModel.setId(resultSet.getInt("id"));
                userModel.setName(resultSet.getString("user_name"));
                userModel.setHobby(resultSet.getString("user_hobby"));
                userModel.setNickName(resultSet.getString("user_nickname"));
                userModel.setPassword(resultSet.getString("user_pwd"));
                userModel.setAccount(resultSet.getString("user_account"));
                userModel.setGender(resultSet.getInt("user_gender") == 1 ? "男" : "女");
                userModel.setMate(resultSet.getString("user_mate"));
                userModel.setPhone(resultSet.getString("user_phone"));
                userModel.setBirthday(resultSet.getDate("user_birthday"));
                userModel.setIsAdmin(resultSet.getInt("user_is_admin"));
                userModel.setEnable(resultSet.getInt("enable"));
                userModel.setWeddingAnniversary(resultSet.getDate("user_wedding_anniversary"));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JDBCUtil.close();
        }

        return userModel;
    }
}
