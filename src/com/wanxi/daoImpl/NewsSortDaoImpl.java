package com.wanxi.daoImpl;

import com.wanxi.dao.NewsSortDao;
import com.wanxi.model.NewsSortModel;
import com.wanxi.util.JDBCUtil;
import com.wanxi.util.ModelUtil;
import com.wanxi.util.OtherUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class NewsSortDaoImpl implements NewsSortDao {
    @Override
    public List<NewsSortModel> getNewsSorts() {
        String sql="select * from news_sort";
        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return ModelUtil.getListNewsSortModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
          JDBCUtil.close();
        }
        return null;
    }

    @Override
    public List<NewsSortModel> getNewsSortsForIndex() {
        String sql="select * from news_sort where enable=1";
        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return ModelUtil.getListNewsSortModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
          JDBCUtil.close();
        }
        return null;
    }

    @Override
    public List<NewsSortModel> getNewsSortsByKey(String key) {
        key="%"+key+"%";
        String sql="select * from news_sort where " +
                "id like ? or " +
                "news_sort_type like ? or " +
                "news_sort_prikey like ?";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,key);
            preparedStatement.setString(2,key);
            preparedStatement.setString(3,key);
            ResultSet resultSet = preparedStatement.executeQuery();
            return ModelUtil.getListNewsSortModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }

    @Override
    public List<NewsSortModel> getNewsSortsByKeyWithPage(String key, int startPage, int pageSize, int isPage) {
        return null;
    }

    public List<NewsSortModel> getNewsSortsByKeyWithPage(String key,int startRows,int pageSize) {
        key="%"+key+"%";
        String sql="select * from news_sort where " +
                "id like ? or " +
                "news_sort_type like ? or " +
                "news_sort_prikey like ? " +
                "limit ?,?";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,key);
            preparedStatement.setString(2,key);
            preparedStatement.setString(3,key);
            preparedStatement.setInt(4,startRows);
            preparedStatement.setInt(5,pageSize);
            ResultSet resultSet = preparedStatement.executeQuery();
            return ModelUtil.getListNewsSortModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }

    @Override
    public NewsSortModel getNewsSortsById(int id) {
        String sql="select * from news_sort where id="+id;
        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return ModelUtil.getListNewsSortModelFromResultSet(resultSet).get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }

    @Override
    public int saveNewsSort(NewsSortModel newsSortModel) {

        String sql="insert into news_sort(news_sort_type,news_sort_prikey,enable)" +
                "values(?,?,?)";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,newsSortModel.getType());
            preparedStatement.setString(2,newsSortModel.getPriKey());
            preparedStatement.setInt(2,newsSortModel.getEnable());
            int i = preparedStatement.executeUpdate();
            OtherUtil.jeadge(i,0,"新闻类型");
            return i;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return 0;
    }

    @Override
    public int updateNewsSort(NewsSortModel newsSortModel) {
        String sql="update news_sort set " +
                "news_sort_type=?," +
                "news_sort_prikey=?," +
                "enable=?" +
                " where id=?";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,newsSortModel.getType());
            preparedStatement.setString(2,newsSortModel.getPriKey());
            preparedStatement.setInt(3,newsSortModel.getEnable());
            preparedStatement.setInt(4,newsSortModel.getId());
            int i = preparedStatement.executeUpdate();
            return i;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return 0;
    }

    @Override
    public int delNewsSort(int id) {
        String sql="delete from news_sort where id="+id;
        return JDBCUtil.executeUpdate(sql);
    }
}
