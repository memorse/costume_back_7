package com.wanxi.daoImpl;

import com.wanxi.dao.NavigatorDao;
import com.wanxi.model.NavigatorModel;
import com.wanxi.util.JDBCUtil;
import com.wanxi.util.ModelUtil;
import com.wanxi.util.OtherUtil;
import sun.plugin.javascript.navig.Navigator;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class NavigatorDaoImpl implements NavigatorDao {
    @Override
    public List<NavigatorModel> getNavigators() {
        String sql="select * from navigator";

        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return  ModelUtil.getListNavigatorModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }

    /**
     * 首页展示导航
     * @return
     */
    @Override
    public List<NavigatorModel> getNavigatorsForIndex() {
        String sql="select * from navigator where enable=1";

        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return  ModelUtil.getListNavigatorModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }

    @Override
    public List<NavigatorModel> getNavigatorsByKey(String key) {
        key="%"+key+"%";
        String sql="select * from navigator where " +
                "id like ? or " +
                "nav_name like ? or " +
                "nav_target like ? or " +
                "nav_level like ? or " +
                "nav_parent like ?";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,key);
            preparedStatement.setString(2,key);
            preparedStatement.setString(3,key);
            preparedStatement.setString(4,key);
            preparedStatement.setString(5,key);
            ResultSet resultSet = preparedStatement.executeQuery();
            return ModelUtil.getListNavigatorModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }

    @Override
    public List<NavigatorModel> getNavigatorsByKeyWithPage(String key, int startPage, int pageSize, int isPage) {
        return null;
    }


    public List<NavigatorModel> getNavigatorsByKeyWithPage(String key,int startRows,int pageSize) {
        key="%"+key+"%";
        String sql="select * from navigator where " +
                "id like ? or " +
                "nav_name like ? or " +
                "nav_target like ? or " +
                "nav_level like ? or " +
                "nav_parent like ? " +
                "limit ?,?";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,key);
            preparedStatement.setString(2,key);
            preparedStatement.setString(3,key);
            preparedStatement.setString(4,key);
            preparedStatement.setString(5,key);
            preparedStatement.setInt(6,startRows);
            preparedStatement.setInt(7,pageSize);
            ResultSet resultSet = preparedStatement.executeQuery();
            return ModelUtil.getListNavigatorModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }

    @Override
    public int saveNavigator(NavigatorModel navigatorModel) {

        String sql="insert into navigator(nav_name,nav_target,nav_level,nav_parent,enable)" +
                "values(?,?,?,?,?)";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,navigatorModel.getName());
            preparedStatement.setString(2,navigatorModel.getTarget());
            preparedStatement.setInt(3,navigatorModel.getLevel());
            preparedStatement.setInt(4,navigatorModel.getParent());
            preparedStatement.setInt(5,navigatorModel.getEnable());
            int i = preparedStatement.executeUpdate();
            OtherUtil.jeadge(i,0,"导航");
            return i;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return 0;
    }

    @Override
    public int updateNavigator(NavigatorModel navigatorModel) {
        String sql="update navigator set " +
                "nav_name=?," +
                "nav_target=?," +
                "nav_level=?," +
                "nav_parent=?," +
                "enable=? " +
                "where id=?";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,navigatorModel.getName());
            preparedStatement.setString(2,navigatorModel.getTarget());
            preparedStatement.setInt(3,navigatorModel.getLevel());
            preparedStatement.setInt(4,navigatorModel.getParent());
            preparedStatement.setInt(5,navigatorModel.getEnable());
            preparedStatement.setInt(6,navigatorModel.getId());
            int i = preparedStatement.executeUpdate();
            return i;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return 0;
    }

    @Override
    public int delNavigator(int id) {
        String  sql="delete from navigator where id="+id;

        return JDBCUtil.executeUpdate(sql);
    }
}
