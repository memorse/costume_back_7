package com.wanxi.daoImpl;

import com.wanxi.dao.ProductionTypeDao;
import com.wanxi.model.ProductionTypeModel;
import com.wanxi.util.JDBCUtil;
import com.wanxi.util.ModelUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class ProductionTypeDaoImpl implements ProductionTypeDao {
    @Override
    public List<ProductionTypeModel> getProductionTypes() {
        String sql="select * from production_type";
        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return ModelUtil.getListProductionTypeModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }

    @Override
    public ProductionTypeModel getProductionTypeById(int id) {
        String  sql="select * from production_type where id ="+id;
        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return ModelUtil.getListProductionTypeModelFromResultSet(resultSet).get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }
}
