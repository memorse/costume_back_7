package com.wanxi.daoImpl;

import com.wanxi.dao.ProductionDao;
import com.wanxi.model.ProductionModel;
import com.wanxi.util.JDBCUtil;
import com.wanxi.util.ModelUtil;
import com.wanxi.util.OtherUtil;
import jdk.nashorn.internal.scripts.JD;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductionDaoImpl implements ProductionDao {
    @Override
    public List<ProductionModel> getProductions() {
        String sql="select * from production";
        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return ModelUtil.getListProductionModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }

    @Override
    public List<ProductionModel> getProductionsForIndex() {
        String sql="select * from production where enable=1";
        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return ModelUtil.getListProductionModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }

    @Override
    public List<ProductionModel> getProductionsBykey(String key) {
        key="%"+key+"%";
        String sql="select * from production where " +
                "id like ? or " +
                "pro_name like ? or " +
                "pro_price like ? or " +
                "pro_photo like ? or " +
                "pro_designer like ?";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,key);
            preparedStatement.setString(2,key);
            preparedStatement.setString(3,key);
            preparedStatement.setString(4,key);
            preparedStatement.setString(5,key);
            ResultSet resultSet = preparedStatement.executeQuery();
            return ModelUtil.getListProductionModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }
    @Override
    public List<ProductionModel> getProductionsBykeyWithPage(String key, int startPage, int pageSize, int isPage) {
        return null;
    }

    public List<ProductionModel> getProductionsBykeyWithPage(String key,int startPage,int pageSize) {
        key="%"+key+"%";
        String sql="select * from production where " +
                "id like ? or " +
                "pro_name like ? or " +
                "pro_price like ? or " +
                "pro_photo like ? or " +
                "pro_designer like ? " +
                "limit ?,?";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,key);
            preparedStatement.setString(2,key);
            preparedStatement.setString(3,key);
            preparedStatement.setString(4,key);
            preparedStatement.setString(5,key);
            preparedStatement.setInt(6,startPage);
            preparedStatement.setInt(7,pageSize);
            ResultSet resultSet = preparedStatement.executeQuery();
            return ModelUtil.getListProductionModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }

    @Override
    public int saveProduction(ProductionModel pro) {

        String sql="insert into production(pro_name,pro_price,pro_photo,pro_designer,enable)" +
                "values(?,?,?,?,?)";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,pro.getProName());
            preparedStatement.setString(2,pro.getProPrice());
            preparedStatement.setString(3,pro.getProPhoto());
            preparedStatement.setInt(4,pro.getDesigner().getId());
            preparedStatement.setInt(5,pro.getEnable());
            int i = preparedStatement.executeUpdate();
            OtherUtil.jeadge(i,0,"产品");
            return i;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return 0;
    }

    @Override
    public int updateProduction(ProductionModel pro) {
            String sql="update production set " +
                    "pro_name=?," +
                    "pro_price=?," +
                    "pro_photo=?," +
                    "pro_designer=?," +
                    "enable=?," +
                    "pro_type=? " +
                    "where id =?";
        try {
            PreparedStatement preparedStatement = JDBCUtil.getPreparedStatement(sql);
            preparedStatement.setString(1,pro.getProName());
            preparedStatement.setString(2,pro.getProPrice());
            preparedStatement.setString(3,pro.getProPhoto());
            preparedStatement.setInt(4,pro.getDesigner().getId());
            preparedStatement.setInt(5,pro.getEnable());
            preparedStatement.setInt(6,pro.getType().getId());
            preparedStatement.setInt(7,pro.getId());
            int i = preparedStatement.executeUpdate();
            return i;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return 0;
    }

    @Override
    public int delProduction(int id) {
        String sql="delete from production where id="+id;
        return JDBCUtil.executeUpdate(sql);
    }
@Override
    public List<ProductionModel> getProductionsByType(int type_id) {
        String sql="select * from production where pro_type="+type_id;
        try {
            ResultSet resultSet = JDBCUtil.executeQuery(sql);
            return ModelUtil.getListProductionModelFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtil.close();
        }
        return null;
    }
}
