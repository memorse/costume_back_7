package com.wanxi.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.wanxi.dao.NewsSortDao;

import java.util.Date;
import java.util.List;

public class NewsModel {
    private int id;
    private String title;
    private String content;
    private String imgRoute;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date pubTime;
    @JSONField(serialzeFeatures = SerializerFeature.DisableCircularReferenceDetect)
    private NewsSortModel newsSort;
    private int enable;

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImgRoute() {
        return imgRoute;
    }

    public void setImgRoute(String imgRoute) {
        this.imgRoute = imgRoute;
    }

    public NewsSortModel getNewsSort() {
        return newsSort;
    }

    public void setNewsSort(NewsSortModel newsSort) {
        this.newsSort = newsSort;
    }

    public Date getPubTime() {
        return pubTime;
    }

    public void setPubTime(Date pubTime) {
        this.pubTime = pubTime;
    }
}
