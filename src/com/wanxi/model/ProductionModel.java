package com.wanxi.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.serializer.SerializerFeature;

public class ProductionModel {
    private int id;
    private String proName;
    private String proPrice;
    private String proPhoto;
    @JSONField(serialzeFeatures = SerializerFeature.DisableCircularReferenceDetect)
    private ProductionTypeModel type;
    @JSONField(serialzeFeatures = SerializerFeature.DisableCircularReferenceDetect)
    private DesignerModel designer;
    private int enable;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getProPrice() {
        return proPrice;
    }

    public void setProPrice(String proPrice) {
        this.proPrice = proPrice;
    }

    public String getProPhoto() {
        return proPhoto;
    }

    public void setProPhoto(String proPhoto) {
        this.proPhoto = proPhoto;
    }

    public ProductionTypeModel getType() {
        return type;
    }

    public void setType(ProductionTypeModel type) {
        this.type = type;
    }

    public DesignerModel getDesigner() {
        return designer;
    }

    public void setDesigner(DesignerModel designer) {
        this.designer = designer;
    }

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }
}
