package com.wanxi.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.serializer.SerializerFeature;

public class CompanyModel {
    private int id;
    private String name;
    private String phone;
    private String qq;
    private String address;
    private String ad;
    private String email;
    @JSONField(serialzeFeatures={SerializerFeature.WriteNullStringAsEmpty})
    private String logo;
    @JSONField(serialzeFeatures={SerializerFeature.WriteNullStringAsEmpty})
    private String qrCode;
    private String introduce;
    private Integer enable;

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    @Override
    public String toString() {
        return "CompanyModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", qq='" + qq + '\'' +
                ", address='" + address + '\'' +
                ", ad='" + ad + '\'' +
                ", email='" + email + '\'' +
                ", logo='" + logo + '\'' +
                ", qrCode='" + qrCode + '\'' +
                ", introduce='" + introduce + '\'' +
                ", enable=" + enable +
                '}';
    }
}
