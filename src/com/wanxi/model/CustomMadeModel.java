package com.wanxi.model;

public class CustomMadeModel {
    private int id;
    private String stepLogo;
    private String stepName;
    private int stepOrder;
    private String stepDetail;
    private int enable;

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStepLogo() {
        return stepLogo;
    }

    public void setStepLogo(String stepLogo) {
        this.stepLogo = stepLogo;
    }

    public String getStepName() {
        return stepName;
    }

    public void setStepName(String stepName) {
        this.stepName = stepName;
    }

    public int getStepOrder() {
        return stepOrder;
    }

    public void setStepOrder(int stepOrder) {
        this.stepOrder = stepOrder;
    }

    public String getStepDetail() {
        return stepDetail;
    }

    public void setStepDetail(String stepDetail) {
        this.stepDetail = stepDetail;
    }

    @Override
    public String toString() {
        return "CustomMadeModel{" +
                "id=" + id +
                ", stepLogo='" + stepLogo + '\'' +
                ", stepName='" + stepName + '\'' +
                ", stepOrder=" + stepOrder +
                ", stepDetail='" + stepDetail + '\'' +
                ", enable=" + enable +
                '}';
    }
}
