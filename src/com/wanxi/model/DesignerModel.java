package com.wanxi.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.serializer.SerializerFeature;


public class DesignerModel {
    private int id;
    private String name;
    private String gender;
    private String introduce;
    @JSONField(serialzeFeatures = SerializerFeature.DisableCircularReferenceDetect)
    private DesignerLevelModel level;
    private String photo;
    private int enable;

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public DesignerLevelModel getLevel() {
        return level;
    }
    public void setLevel(DesignerLevelModel level) {
        this.level = level;
    }
    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @Override
    public String toString() {
        return "DesignerModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", introduce='" + introduce + '\'' +
                ", level=" + level +
                ", photo='" + photo + '\'' +
                ", enable=" + enable +
                '}';
    }
}
