package com.wanxi.model;

public class DesignerLevelModel {

    private int id;
    private String levelName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    @Override
    public String toString() {
        return "DesignerLevelModel{" +
                "id=" + id +
                ", levelName='" + levelName + '\'' +
                '}';
    }
}
