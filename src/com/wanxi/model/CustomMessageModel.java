package com.wanxi.model;

public class CustomMessageModel {
    private int id;
    private String wifeMessage;//妻子留言
    private String husMessage;//丈夫留言
    private String firstName;//姓氏
    private String photo;
    private int enable;

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWifeMessage() {
        return wifeMessage;
    }

    public void setWifeMessage(String wifeMessage) {
        this.wifeMessage = wifeMessage;
    }

    public String getHusMessage() {
        return husMessage;
    }

    public void setHusMessage(String husMessage) {
        this.husMessage = husMessage;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
