package com.wanxi.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.serializer.SerializerFeature;

import java.util.Arrays;
import java.util.Date;

public class UserModel {
    private int id;
    private String account;
    private String password;
    private String name;
    private String nickName;
    private String phone;
    private String gender;
    private String hobby;
    private String mate;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date birthday;

    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date weddingAnniversary;
    private int enable;
    private int isAdmin;

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }
    public int getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(int isAdmin) {
        this.isAdmin = isAdmin;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public String getMate() {
        return mate;
    }

    public void setMate(String mate) {
        this.mate = mate;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Date getWeddingAnniversary() {
        return weddingAnniversary;
    }

    public void setWeddingAnniversary(Date weddingAnniversary) {
        this.weddingAnniversary = weddingAnniversary;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "id=" + id +
                ", account='" + account + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", nickName='" + nickName + '\'' +
                ", gender='" + gender + '\'' +
                ", hobby=" + hobby +
                ", mate='" + mate + '\'' +
                ", birthday=" + birthday +
                ", weddingAnniversary=" + weddingAnniversary +
                '}';
    }
}
